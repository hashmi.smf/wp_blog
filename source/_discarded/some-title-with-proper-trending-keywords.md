title: Title with proper trending keywords
author: Westpointoptical
tags:
  - SEO tips for blog
categories:
  - westpoint seo
date: 2021-03-13 13:54:00
---
![astronaut performing space walk](/images/space_walk.jpg)

##### Every post title is a h1 tag

the description of the post is generally in p tag

**for seo make some bold text so that google crawl pages will uplift the domain rating**

> add this block quote as customer testimonials

make some ordered listings

1.listing1
2.listing2

make some unordered listing

- unordered listing1
- unordered listing2

[link should have a title like this](https://westpointoptical.ca)

| add | tables |
| -------| ----------- |
| Header    | Title |
| Paragraph | Text |


~~The world is flat.~~ We now know that the world is round.