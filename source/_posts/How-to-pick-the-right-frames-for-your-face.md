---
title: How to pick the right frame for your face
author: Westpointoptical
tags:
  - frames
  - suitable frames
  - eyeglass frame
  - eyeglasses
  - glasses online
  - glasses frames
  - online eyeglasses
  - eye exam near me
categories: How to pick the right frame for your face
date: 2021-03-15 20:36:00
---
<span style=" font-size:17pt;">**Face Shape** </span>  
Is your face round, oval, square, diamond or heart-shaped? The shape of your face will help you determine which frames enhance your look.  

![face shapes](/images/face-shape-guide-main-faces-glasses.jpg)
- <span style=" font-size:15pt;">**Round Face:**</span>    
  **Eyeglass frames** that are square or rectangular tend to be wider than a round face. This quality can enhance your face by making it appear slimmer and longer, adding balance to your round features.  
  
  **Frames to Avoid:** Rimless frames, round frames and small frames will accentuate the roundness, making your round face look even rounder.
 
- <span style=" font-size:15pt;">**Oval Face:** </span>    
Frames that suit an oval face have a strong bridge, are wider than the broadest part of the face and are geometric in shape. 
 
  **Frames to Avoid:** Eyeglasses that are overly large and cover up more than half of your face will throw off the natural balance and symmetry of the oval face.

- <span style=" font-size:15pt;">**Square Face:**     </span>  
Eyeglasses that soften the angularity and sit high on the bridge of the nose look best on square faces. Oval or round eyeglasses will balance and add a thinner appearance to the angles of a square face.
  
  **Frames to Avoid:** Angular and boxy eyeglass frames will sharpen and draw attention to your angular features, making a square face appear bulky.
 
- 
<span style=" font-size:15pt;">**Diamond Face:**    </span>   
 Play up a narrow forehead and chin with **eyeglasses** what sweep up or are wider than the cheekbones, such as cat eye glasses and oval frames. These frames will accentuate your cheekbones and delicate features. 
  
  **Frames to Avoid:** Boxy and narrow frames will accentuate the width of your cheeks, drawing attention to your narrow features rather than enhancing them. 
  
- <span style=" font-size:15pt;">**Heart-Shaped Face:**      </span>   
  **Frames** that balance the width of the forehead with the narrowness of the chin are ideal. Eyeglasses with low-set temples and bottom heavy frame lines will add width to that narrower part of your face. 
 
 
  <p style="text-align: center;  font-size:22pt;"> Glasses for All Kinds of Shapes </p>

<span style=" font-size:15pt;">**Here’s the truth about face shapes:**     </span>  

Almost nobody is a perfect heart, circle, square, or any other narrowly defined category. Most faces are a combination of a few different shapes: rounded chins and tall foreheads, angular features and tapered jaws, and so on.

 Once you’ve recognized your face shape, you can find which frame shape will bring a natural balance to your face. This guide will help identify the type of glasses shape you should look at when browsing online or in store.
 
<p style="text-align: center;  font-size:22pt; "> Fitting Tips </p>  
The most important thing to remember when choosing glasses for round face shapes is to select frames that work to add angles. They look great in bold, angular <strong>glasses</strong> with clean lines.  
   
     
<p style="text-align: center;  font-size:22pt; "> Below are a few tips to keep in mind when you search. </p>

<ul>
  <li>
    <span style=" font-size:15pt;"><strong>Rectangular frames:</strong></span>
    <p>Break up the face structure, which can make your face appear longer and thinner. </p>
  </li>
</ul>  
  
  <ul>
  <li>
    <span style=" font-size:15pt;"><strong>Angular and geometric frames:</strong></span>
    <p>Add some sharper, more distinct lines to your face in order to create a balance. </p>
  </li>
</ul>  
  
   <ul>
  <li>
    <span style=" font-size:15pt;"><strong>Upswept frames such as cat-eye or D-frame:</strong></span><p>If you have full-bodied curvy cheeks, try to draw attention to them while simultaneously placing the focus on your eyes. </p>
  </li>
</ul>
  
  

