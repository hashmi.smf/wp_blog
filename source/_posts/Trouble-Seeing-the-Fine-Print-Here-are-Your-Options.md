---
title: Trouble Seeing the Fine Print? You May Need Reading Glasses!
author: Westpoint Optical
tags: 
  - fine print
  - presbyopia	
  - presbyopia surgery	
  - presbyopia symptoms	
  - presbyopia treatment	
  - what is presbyopia	
  - presbyopic glasses	
  - presbyopia cure	
  - laser eye surgery for reading	
  - bifocal	
  - progressive lenses
  - presbyopia correction	
  - vitamins for eyesight	
  - presbyopia symptoms	
  - lasik eye surgery toronto	
  - eye exam near me
  - optical in brampton
  - eye infection
  - westpoint optical eye wear
  - trifocal lenses

Categories: Trouble Seeing the Fine Print? Here are Your Options
date: 2021-03-30 19:34:33
---


![](/images/trouble-seeing-fine-print.jpg)

Do you ever find yourself struggling to read menus? Have you found that you need to double the size of the font on your phone? Are you running out of arm length because you are already fully extending your arm to read anything small?

If you do, and you are over the age of 40, chances are you have presbyopia.

## What is Presbyopia?

Presbyopia is a normal aging change in the eye. The lens, which controls the focusing power of the eye, loses its flexibility as we get older. This makes it difficult to focus up close. Someone with presbyopia may find they have a difficult time reading fine print, especially when they don't have good lighting. For most people, this process occurs around the age of 40 and continues until around the age of 60. If uncorrected, presbyopia can lead to headaches, eyestrain, and fatigue after prolonged periods of reading. Unfortunately, there is no way to prevent or stop presbyopia, but the good news that it can be picked up during an eye exam and easily corrected with glasses.

## Bifocals, Trifocals, and Progressives...

Presbyopia can easily be corrected with reading glasses or bifocals. These glasses provide magnification that allows your eyes to relax, reducing the focusing power you need for something up close. Some people may be able to get by with readers that you can buy at the drug store, but most of our patients opt for prescription reading or bifocal glasses. Bifocal lenses have distance prescription at the top of the lens and reading prescription at the bottom. A trifocal lens has an additional portion of the lens that provides correction for an arm's length or computer distance. A progressive lens blends all of these prescriptions together to form a "no line" trifocal. Our lens experts are trained to help you choose the style that is best for you and your lifestyle.

## Myths About Presbyopia

**"I don't want to wear my reading glasses - they will make my vision worse!"**
This is completely false! Many people have this misconception because once you first start needing reading glasses, near vision will continue to worsen over time. This process occurs regardless of if you wear reading glasses or not. Going without reading glasses only leads to more eye strain and fatigue.

**"I can't wear contacts once I need reading glasses"**
Again, completely false! Contact lens companies are always coming out with new technologies to help correct vision for the 40+ crowd. Especially because patients over 40 are much more active than their parents were at their age, contact lenses that correct for both distance and near vision are more desirable than ever. At Westpoint Optical, we carry a large selection of bifocal contact lenses. There is also the option of using contact lenses to correct one eye for distance and one eye for near, called monovsion.

**"I am never going to be able to adjust to wearing a bifocal/progressive!"**
While it is true that bifocals have an adaptations period, most people are able to successfully adjust to these new lenses. When it comes to these complex lens designs, it is important to have accurate measurements. These measurements ensure that the lenses are customized to fit your eyes in your particular frame. It is also true that you get what you pay for when it comes to the design and quality of the lens. Premium lens products set you up for the greatest success by maximizing visual clarity and minimizing distortions.

Still have more questions about reading glasses and the options available?  Make an appointment with [Westpoint Optical](https://westpointoptical.ca/) to figure out the visual correction that best fits your lifestyle!


