---
title: Keeping an Eye on Cataracts
author: Westpoint Optical
tags:
 - eye cataract
 - eye cataract operation
 - eye cataract surgery
 - eye cataract operation cost in india
 - eye cataract treatment without surgery
 - what is eye cataract
 - eye cataract surgery cost
 - eye cataract symptoms
 - eye cataract meaning
 - left eye cataract
 - eye cataract surgery by laser
 - eye cataract surgery recovery time
 - operation of eye cataract
 - eye cataract surgery cost in hyderabad
 - eye cataract home remedies
 - eye cataract surgery cost in india
categories: Keeping an Eye on Cataracts
date: 2021-04-07 18:07:55
---
![eye on cataracts](/images/Keeping-an-Eye-on-Cataracts.jpg)

Cataracts are the most widely recognized reason for vision misfortune because of maturing, as of now distressing 20 million individuals around the world. This condition happens when clear protein in the characteristic focal point called crystallins becomes misty, making it shady. This dissipates light beams going through the eye making obscured vision.

What is a waterfall? The part behind the iris (shaded piece) of our eyes is known as the focal point. As individuals age or have certain ailments, the typically clear focal point can create shady spots, making us have visual issues. As the waterfall develops, vision turns out to be more terrible. Cataracts influence over half of Americans beyond 65 years old and can begin as ahead of schedule as age 40 – they are the main source of visual deficiency on the planet. Luckily, for us who live in the United States, medical procedure is promptly accessible, for the individuals who need it, to forestall vision misfortune.

## Types of cataracts include:
**Age-related cataracts:** These Cataracts structure due to maturing changes.

**Congenital cataracts:** Infants are now and again brought into the world with Cataractsbecause of intrauterine disease or because of hereditary irregularities.

**Secondary cataracts:** These create because of other ailments, similar to diabetes, or openness to harmful substances, certain medications (like corticosteroids or diuretics), bright light, or radiation.

**Traumatic cataracts:** These structures after a physical issue to the eye.

**Radiation cataract:** Cataracts can create after openness to certain kinds of radiation.    

## Causes Of Cataract
Cataracts are brought about by the breakdown of proteins in the focal point of your eye. Most Cataracts happen as a result of old enough related changes in your eye, however, different issues can make them structure quicker, including:
 - Smoking.
 - Drinking too much alcohol.
 - Spending too much time in the sun without sunglasses.
 - Diabetes.
 - Serious eye injury.
 - Eye surgery to treat glaucoma or another eye condition.
 - Taking steroids for a long time.
 - Radiation treatment for cancer and other health conditions.

![Symptoms of Cataracts](/images/Symptoms-of-Cataracts.jpg)

## Symptoms of Cataracts
Cataracts usually form slowly and cause few symptoms until they noticeably block light.
 - Vision that is cloudy, blurry and foggy.
 - Progressive nearsightedness in older people often called “second sight” because although their distance vision is getting worse, they may no longer need reading glasses.
 - Changes in the way you see color.
 - Problems driving at night such as glare from oncoming headlights.
 - Problems with glare during the day.
 - Double vision
 - Change in glasses prescription

Cataracts for the most part grow gradually, and new glasses, more splendid lighting, against glare shades, or amplifying focal points can help from the start. The medical procedure is likewise a choice to eliminate the overcast focal point and supplant it with a counterfeit focal point. Wearing shades and a cap with an edge to obstruct bright daylight may assist with postponing Cataracts.

## Cigarette smoking, air pollution, and heavy drinking can increase the risk of getting cataract:
Activity Plan – The early distinguishing proof of outwardly huge cataracts and giving admittance to waterfall medical procedures are among the two most significant activities to decrease avoidable visual impairment because of cataracts. Expanding the waterfall careful rate (CSR) and waterfall careful inclusion (CSC) are keys to expanding the spread and improving openness to waterfall medical procedure. Preparing subordinate staff to recognize cataracts and building an open reference framework to really focus on people with cataracts improves the compass and viability of waterfall screening programs. Likewise, great residency preparation in waterfall medical procedures will help increment the pool of eye specialists performing effective cataract medical procedures.

The treatment of cataracts is careful and fruitful in reestablishing sight. The overcast focal point is eliminated and supplanted by a fake intraocular focal point.

Visit [westpointoptical.ca](https://westpointoptical.ca/contact.html) or call **905-488-1626** 
