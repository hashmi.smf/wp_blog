---
title: A Look Behind Sleeping Eyes
author: Westpoint Optical
tags:
  - sleeping eye mask
  - eyelids
  - itchy eyelids
  - swollen eyelids
  - types of eyelids
  - dry eyelids
  - rem sleep
  - how to get more rem sleep
  - rem sleep cycle
  - too much rem sleep
  - how much rem sleep do you need
  - rem sleep behavior disorder
  - westpoint optical eyewear
  - optical near me
  - sunglasses
  - eye sight
  - eye exam cost brampton
  - optometrist near me
  - eye test
  - eye health tips
categories: A Loook Behind Sleeping Eyes
Date: 2021-03-26 13:39:50
---

Have you at any point thought about what your eyes do when you at last close them following a difficult day of visual preparation and incitement? We should investigate what occurs behind your shut covers when your head hits the pad.

First and foremost, when your eyes are shut, they do keep on working in a restricted design with the capacity to detect light. This clarifies why a brilliant light being turned on or the sun ascending in the first part of the day can awaken you while lying in a dull room will help you rest.

During rest, your eyes don't send visual information or data about pictures to your mind. Truth be told, it requires right around 30 seconds for the association between your eyes and your mind to reboot when you awaken. This is the reason it's frequently hard to see total and clear pictures when you first wake up.

Our bodies go through five periods of rest known as stages 1, 2, 3, 4, (which together are called Non-REM), and REM (quick eye development) rest. During a normal rest cycle, you progress from stage 1 to 4 then REM and afterward begin once again. Right around 50% of our all-out rest time is spent in stage 2 rest, while 20% is spent in REM rest and the leftover 30% in different stages. During stage 1, your eyes move gradually, opening and shutting somewhat; be that as it may, the eyes are then still from stages 2-4 when rest is more profound.

During REM rest, your eyes move around quickly in the scope of bearings however don't send any visual data to your cerebrum. Researchers have found that during REM rest the visual cortex of the cerebrum, which is liable for handling visual information, is dynamic. Nonetheless, this action fills in as a feature of a memory framing or building up work that plans to merge your memory with encounters from the day, instead of preparing visual data that you see. This is likewise when the vast majority dream.

As for your eyelids, they cover your eyes and function as a shield protecting them from light. They also help preserve moisture on the cornea and prevent your eyes from drying out while your body is resting.

To put it plainly, while your eyes do move around during rest, they are not effectively preparing visual symbolism. Shutting your eyelids and resting basically offers your eyes a reprieve. Close eye re-energizes your eyes, setting them up to help you see the following day.