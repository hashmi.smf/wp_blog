---
title: Got a Shiner!
author: Westpoint Optical
tags:
  - pair of glasses
  - perfect glasses	
  - types of eyeglasses	
  - pair of glasses	
  - pair of sunglasses	
  - eyeglass frames for men face shape	
  - magnetic glasses	
  - 2 pairs	
  - best pair	
  - which frame suits me	
  - cost of spectacles	
  - wearing spectacles side effects	
  - glasses synonyms	
  - eye exam near me	
  - optics near me	
  - spectacles parts	
Categories: Got a Shiner!
date: 2021-05-07 11:54:55
---
![Shiner](/images/Got-a-Shiner!.jpg)
### What Exactly Is a Black Eye?
A bruised eye, otherwise called a periorbital hematoma, is generally not a physical issue to the real eye (which is the reason it is classified as "periorbital"- around the eye). It normally happens when there is a physical issue to the face or the eye attachment which causes seeping underneath the skin and wounding. The expression, "bruised eye" comes from the dull shading of the wounding that happens under the skin around the eye. 

At the point when a dull power hits the eye attachment, this can make vessels in the space burst, causing discharging, otherwise called a hematoma. This blood can gather in the eye attachment and as it is reabsorbed into the encompassing tissues, the shades of the wounding start to change. That is the reason you will regularly see the shading of the bruised eye going from a dull purplish-red tone to caramel and afterward yellow. 

Now and again alongside the outer wounding, you may likewise see a limited quantity of seeping on the white surface of the eye, which is known as a subconjunctival drain. This is the point at which the minuscule veins on the white sclera are broken and spill blood. It's by and large innocuous however in some cases looks more alarming to the patient than the bruised eye does. This condition will likewise reabsorb all alone and is not something to be worried about.

While most bruised eyes can look really genuine because of the emotional shading, a simple bruised eye will regularly mend inside seven days to ten days. On the off chance that it doesn't, there could be a more major issue, for example, a bone crack or an orbital victory break. This could give confined eye development, particularly if turning upward or down, and deadness of the cheek as well as an upper lip on a similar side as the bruised eye. The eye may even seem depressed. Further, if there is seeping inside the genuine eye (called a hyphema) or floaters or blazes in the vision, at that point it is certainly prudent to see your eye specialist as quickly as time permits. These could be indications of more genuine harm particularly corneal or retinal harm and can prompt vision misfortune.

### Treatment for a Black Eye
Typically, the best treatment for a bruised eye is to apply a virus pack (or shockingly better, a sack of frozen vegetables, which is more adaptable and can adjust to the shapes of the face) straightforwardly on the space. The virus will decrease growth and tighten vessels to diminish interior seeping too. Apply the cold for around 15-20 minutes consistently. In the event that there is torment, over-the-counter agony meds can help.

If however, you experience any of the following symptoms, you should seek medical attention:

 - Blood on the surface of the eye or a visible incision on the eye
 - Vision changes such as double vision, blurred vision, loss of vision or the appearance of floaters
 - Loss of consciousness, dizziness or fainting
 - Loss of eye movement
 - Persistent headaches
 - Blood or fluids coming from the ears or nose
 - Vomiting
 - Signs of infection such as excessive swelling, pus, redness or a fever
 - Severe pain

Notwithstanding dull injury, bruised eyes can be brought about by sinus contaminations, nasal or eye a medical procedure or different diseases in the space like teeth diseases or cellulitis (genuine contamination that can happen around the eyes). A skull break can likewise make the two eyes become dark, now and again known as raccoon eyes.

Except if you notice any serious indications you can have confidence that your bruised eye is a wound actually like elsewhere on the body and with a little consideration, rest, and persistence, it will clear up instantly.