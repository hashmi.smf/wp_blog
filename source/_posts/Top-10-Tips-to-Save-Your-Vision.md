title: Top 10 Tips to Save Your Vision
author: Westpointoptical
tags:
  - top 10 tips for save eye vision
  - sun glasses
  - cataracts
  - health effect
  - healthy food
  - eye exam near me
  - eye exam in brampton
  - westpoint optical eyes wear
  - optical near me
  - eye protection
  - eye diseases
  - vision loss
  - eye blindness
  - eye surgery
  - contact lenses
  - ''
categories:
  - Top 10 Tips to Save Your Vision
date: 2021-03-22 03:20:00
---
More than 20 million Canadians suffer from severe vision loss. While not all eye diseases can be prevented, there are simple steps that everyone can take to help their eyes remain healthy now and reduce their chances of vision loss in the future.

Here are the top 10 tips from the Academy to safeguard your vision:

### 1. Wear Sunglasses 

![Sun Glasses](/images/sunglasses.jpg)

UV blocking sunglasses delay the development of cataracts, since direct sunlight hastens their formation. Sunglasses prevent retinal damage; they also protect the delicate eyelid skin to prevent both wrinkles and skin cancer around the eye, and both cancerous and non-cancerous growths on the eye. Check for 100 percent UV protection: Make sure your sunglasses block 100 percent of UV-A rays and UV-B rays.

### 2. Don't Smoke

![Don't Smoke](/images/dont-smoke.jpg)

Tobacco smoking is directly linked to many adverse health effects, including age-related macular degeneration (AMD). Studies show that current smokers and ex-smokers are more likely to develop AMD than people who have never smoked. Smokers are also at increased risk for developing cataracts.

### 3. Eat Right  

![Eat healthy food](/images/eat-food.jpg)

Vitamin deficiency can impair retinal function. The belief that eating carrots improves vision has some truth, but a variety of vegetables, especially leafy green ones, should be an important part of your diet. Researchers have found people on diets with higher levels of vitamins C and E, zinc, lutein, zeaxanthin, omega-3 fatty acids DHA and EPA are less likely to develop early and advanced AMD.  

### 4. Baseline Eye Exam

![Eye Exam](/images/base-line-eye-exam.jpg)

Adults with no signs or risk factors for eye disease should get a baseline eye disease screening at age 40 — the time when early signs of disease and changes in vision may start to occur. Based on the results of the initial screening, an ophthalmologist will prescribe the necessary intervals for follow-up exams. Anyone with symptoms or a family history of eye disease, diabetes or high blood pressure should see an ophthalmologist to determine how frequently your eyes should be examined.  

### 5. Eye Protection

![Eye Protection](/images/eye-protection.jpg)

An estimated 2.5 million eye injuries occur in the CANADA each year, so it is critical to wear proper eye protection to prevent eye injuries during sports such as hockey and baseball and home projects such as home repairs, gardening, and cleaning. For most repair projects and activities around the home, standard ANSI-approved protective eyewear will be sufficient. Sports eye protection should meet the specific requirements of that sport, these requirements are usually established and certified by the sport's governing body and/or the National Sports Organizations (NSOs).

### 6. Know Your Family History  

![Family History](/images/family-history.jpg)

Many eye diseases cluster in families, so you should know your family's history of eye disease because you may be at increased risk. Age-related eye diseases, including cataracts, diabetic retinopathy, glaucoma and age-related macular degeneration are expected to dramatically increase — from 28 million today to 43 million by the year 2021.  

### 7. Early Intervention 

![Appointment](/images/appointment.jpg)

Most serious eye conditions, such as glaucoma and AMD, are more easily and successfully treated if diagnosed and treated early. Left untreated, these diseases can cause serious vision loss and blindness. Early intervention now will prevent vision loss later.  

### 8. Know Your Eye Care Provider 

![Eye Care Provider](/images/eye-care.jpg)

When you go to get your eyes checked, there are a variety of eye care providers you might see. Ophthalmologists, optometrists and opticians all play an important role in providing eye care services to consumers. However, each has a different level of training and expertise. Make sure you are seeing the right provider for your condition or treatment. Ophthalmologists are specially trained to provide the full spectrum of eye care, from prescribing glasses and contact lenses to complex and delicate eye surgery.  

### 9. Contact Lenses Care  

![Contact lenses](/images/contact-lenses.jpg)

Follow your ophthalmologist's instructions regarding the care and use of contact lenses. Abuse, such as sleeping in contacts that are not approved for overnight wear, using saliva or water as a wetting solution, using expired solutions, and using disposable contact lenses beyond their wear can result in corneal ulcers, severe pain and even vision loss.  

### 10. Be Aware Of Eye Fatigue  

![Eye Fatigue](/images/eye-fatigue.jpg)

If you have eye strain from working at a computer or doing close work, you can follow the 20-20-20 rule: Look up from your work every 20 minutes at an object 20 feet away for twenty seconds. If eye fatigue persists, it can be a sign of several different conditions, such as dry eye, presbyopia, or spectacles with lenses that are not properly centered. See an ophthalmologist to determine why you are having eye fatigue and to receive proper treatment. If you don't already have an ophthalmologist, Find an ophthalmologist in your area. Consumers can submit questions about eye health to an ophthalmologist at Ask an ophthalmologist

Knowing what you have to do is the first step in keeping your eyes and vision safe. Thinking about your eyes and vision and forming the right habits will lead to a lifetime of good vision.


