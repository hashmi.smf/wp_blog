---
title: Under-Pressure; Are you at Risk for Glaucoma?
author: Westpoint Optical
tags:
 - glaucoma
 - glaucoma symptoms
 - glaucoma treatment
 - glaucoma meaning
 - what is glaucoma
 - open angle glaucoma
 - symptoms of glaucoma
 - congenital glaucoma
 - glaucoma surgery
 - glaucoma definition
 - glaucoma causes
 - angle closure glaucoma
 - journal of glaucoma
 - glaucoma eye
 - closed angle glaucoma
 - phacomorphic glaucoma
 - glaucoma test
 - treatment of glaucoma
 - glaucoma images
 - glaucoma disease
 - phacolytic glaucoma
 - causes of glaucoma
 - types of glaucoma

Categories: Under Pressure Are you at Risk for Glaucoma?
date: 2021-04-14 14:09:35
---

![Risk for Glaucoma](/images/Under-Pressure:-Are-you-at-Risk-for-Glaucoma?.jpg)

January is Glaucoma Awareness Month. Glaucoma is a genuine, vision-compromising illness. You can save your eyesight, by knowing current realities. Is it accurate to say that you are in danger of creating glaucoma?

The short answer is yes. Anybody can get glaucoma and along these lines, it is significant for each individual, youthful and old to have an ordinary eye test. Early location and treatment are the lone responses to forestalling the vision weakness and visual impairment that outcome from untreated glaucoma. 

Having said that, there are a couple of elements that put certain people at more serious danger of building up the illness: 

 - **Over age 40:** While glaucoma is known to happen in more youthful patients, even babies, the probability increments with age, especially in those beyond 40 years old. 

 - **Family history:** There is a hereditary factor to the infection, making it almost certain that it will happen when there is a family ancestry. 

 - **Elevated Intraocular Pressure (IOP):** Individuals that have unusually high inward eye pressure (intraocular pressure) have a drastically expanded danger of creating glaucoma and experiencing eye harm it. 

 - **Latino, Asian or African decent:** Evidence plainly shows race is a factor, and people from Latino, African and Asian foundations are at expanded danger of creating glaucoma. African Americans specifically are at a higher danger, will in general create glaucoma at a more youthful age, and have a higher occurrence of visual impairment from the illness. 

 - **Diabetes:** Diabetes, especially when it is uncontrolled, builds the danger of various vision-undermining sicknesses including diabetic retinopathy and glaucoma. 

 - **Eye injury, disease or trauma:** If you have endured a genuine eye injury previously, your danger of glaucoma is expanded. Additionally, other eye conditions like tumors, retinal separation, focal point disengagement, or particular sorts of eye a medical procedure can be factors. 

 - **Extremely high or low blood pressure:** Since glaucoma has to do with the pressing factor inside the eye, strange circulatory strain can add to an expanded danger of the infection. 

 - **Long-term steroid use:** Prolonged utilization of certain corticosteroid prescriptions, like prednisone, especially in eye drop structure, may likewise expand your odds of getting glaucoma. 

 - **Myopia (nearsightedness) or hyperopia (farsightedness):** Poor vision may expand your danger of creating glaucoma. 

Complete eye tests are the way to forestalling vision-compromising infections and visual impairment. A yearly test for each individual can help analyze any eye sickness or any foundational infection from your body that has signs found in the eyes.