---
title: Sjogren’s Awareness – Understanding The Invisible Disease 
author: Westpoint Optical
tags:
 - eye protection
 - eye protection app for pc
 - eye protection glasses
 - eye protection software for pc
 - eye protection glass
 - evil eye protection
 - eye protection app
 - eye protection software
 - eye protection for pc
 - eye protection app for laptop
 - laptop eye protection
 - laptop eye protection screen
 - best eye protection software for pc
 - eye protection for laptop
 - eye protection glass for mobile and computer
 - laptop glasses for eye protection
 - eye protection goggles
 - best eye protection glasses
 - laptop screen guard for eye protection
 - laptop eye protection glasses
 - best sunglasses brand for eye protection
 - eye protection glasses from computer screen
 - laptop eye protection software
 - eye protection from computer
 - eye protection tips
 - eye protection glass for laptop
 - eye protection glasses for computer users
 - glasses for eye protection
 - eye protection screen for laptop
 - computer eye protection glasses
 - eye protection from computer screen
Categories: Sjogren’s Awareness – Understanding The Invisible Disease 
date: 2021-05-06 16:07:22
---
![invisible disease](/images/Sjogren’s-Awareness-Understanding-The-Invisible-Disease.jpg)

With an end goal to get the news out to expand mindfulness about Sjogren's Disease, eye care suppliers are taking a stand in opposition to this difficult-to-analyze condition. 

Sjogren's is a fundamental immune system issue that can influence the entire body. One of the essential side effects is inordinate dryness, especially in the eyes and mouth. Other genuine manifestations incorporate persistent weariness and torment, explicitly in the joints, just as significant organ brokenness. The disorder likewise expands the odds of neuropathies and lymphomas. 

The seriousness of the illness fluctuates incredibly, going from gentle inconvenience to weakening manifestations that can truly debilitate typical working in regular day-to-day existence. Early conclusion and treatment can forestall genuine entanglements and improve personal satisfaction. There is right now no solution for Sjogren's, yet there are medicines for a significant number of the individual side effects. On normal patients are endorsed as many as 8 prescriptions to treat the wide scope of side effects.

### Sjogren’s Awareness
Let’s start with good old Sjogren’s, who I have gotten to know quite well over the past 2 1/2 years. Sjogren’s Syndrome is an autoimmune disease. Autoimmune disease are chronic illnesses in which the body’s immune system attacks healthy cells. They have no known causes and no known cures. Some other major autoimmune diseases include Lupus, Rheumatoid Arthritis, Scleroderma, Celiac Disease, IBD (Crohn’s & Ulcerative Colitis), Graves’ Disease, Hashimoto’s Disease, Psoriasis, Multiple Sclerosis, and Type 1 Diabetes.

Sjogren’s is one of the most prevalent autoimmune diseases. It affects as many as 4 million Americans, 9/10 of which are female (Sjogren’s Syndrome Foundation). You can either have primary Sjogren’s (meaning it is the only autoimmune diagnosis you have) or secondary Sjogren’s (meaning it is associated with another autoimmune disease like Lupus or RA). The defining symptoms of Sjogren’s Syndrome are dry eyes and mouth, as it primarily attacks the body’s moisture producing glands. However, other common symptoms include fatigue and joint pain, and it can potentially affect organs in the body like the kidneys, GI system, lungs, liver, etc.

### Pre-Diagnosis – Looking for Answers
The summer before my junior year of college I was working full time at the daycare where I still work. I would work an 8-4 shift, and then be asleep on the couch when my mom got home from work between 5-6. This may not seem unusual for a 20-year-old, but what about when you factor in that I was also getting at least 8 hours of sleep a night? I was far more tired that I should be.

I had also been getting sick a ton that whole year. I basically had a perpetual cold all winter, and then in the summer I had stomach virus after stomach virus. A fact that I often state with a bizarre sense of pride is that I vomited 20 times when I was age 20 (and only once was alcohol induced, thank you very much). My doctors blew me off, saying my constant sickness was due to me living in a dorm, and then me working with kids in the summer. Those are legitimate points, but my immunity was far lower than all of my counterparts.

### The Diagnosis
I finally received my diagnosis of Sjogren’s from the rheumatologist. He explained that the diagnosis was based on the following factors that I exhibited:

 - Elevated IgG
 - Positive ANA (antinuclear antibodies)
 - High RO and LA
 - Positive Sjogren’s Antibody
 - High SED rate
 - sLow WBC
 - Case of parotitis (inflammation of the parotid salivary gland)
 - Extreme fatigue

There is no hard and fast test for Sjogren’s Syndrome, but rather it is diagnosed based on certain clusters of symptoms. Interestingly, I didn’t develop the typical Sjogren’s dry eyes and mouth until this past fall, 2 years into my diagnosis. At that point, I started noticing my lips would get really dry and it would look like I was actually wearing lip color when I was not. My eye doctor also ran a test of my tear production (which is standard for those with Sjogren’s…it is called a Schirmer’s Test) and found that my tear production was low.

### Living with Sjogren’s
The main obstacle that I have had to deal with while living with Sjogren’s is the crushing fatigue. It is unpredictable, because autoimmune diseases tend to flare. So sometimes my symptoms are well managed, and sometimes they flare up and get a lot worse. When my fatigue is bad, I may have to rest all morning, then take a nap just to be able to get through a 3-4 hour shift at work. If my fatigue is REALLY bad, I may have to call out of work (this usually only happens when I also have another sickness like a cold or virus).

I had to move home from college to deal with this disease. Eventually I began taking online courses in order to finish my undergraduate degree. (Read my post: “A Letter to Myself On The Day I Was Supposed To Graduate College“). I just recently increased my hours at work from 12 hours a week to 16 hours a week. As a very motivated and sometimes type-A person, it can be truly difficult for me to not be going to school or working full time. It can make me feel like I’m being lazy, when in reality I’m doing all that my body can handle. You lose parts of yourself when you gain a chronic illness. (Read my post: “The 5 Stages of Grief After a Diagnosis).

### IBS Awareness
When I was diagnosed with Sjogrens, I realized how wrong I was about CFS when I experienced truly crushing, life-altering levels of fatigue for the first time. When I was diagnosed with IBS last year, I realized just how much gastrointestinal issues (like IBS, Crohn’s, Ulcerative Colitis, etc.) can affect your life.

IBS, or irritable bowel syndrome, affects 10-15% of the population worldwide, and it can range from mild to severe (International Foundation for Functional Gastrointestinal Disorders). Symptoms can range from gas, bloating, and pain to diarrhea or constipation (or a cycle of both). One can either have IBS-C (“C” is for constipation) or IBS-D (with the “D” being for diarrhea). I have IBS-D.