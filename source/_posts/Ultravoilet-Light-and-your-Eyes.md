---
title: Ultravoilet Light and your Eyes
author: Westpoint Optical
tags:
  - ultra violet	
  - sun rays	
  - does uv light kill coronavirus	
  - uv light
  - sun with sunglasses
  - sun glasses
  - eye care
  - westpoint optical stores
  - optical in brampton
  - eye test near me
  - westpoint eyewear
  - eye exam near me
  - contact lenses
  - kids eye wear
  - eye doctor
  - optical near me
  - contact lenses for kids
  - contact lens
  - contact lens case
  - eye sight
  - eye health 
  - westpoint optical eye wear
  - contact focal point
  - eye safety
  - eye doctor

Categories: Ultravoilet Light and your Eyes
date: 2021-03-29 19:14:02
---

## The Sun, UV Light and Your Eyes

Summertime often means long hours in the sun. Most of us remember to protect our skin by applying sunblock, but don't forget that your eyes need protection as well. It is important to start wearing proper eye protection at an early age to shield your eyes from years of ultraviolet exposure.

"UV radiation, whether from natural sunlight or indoor artificial rays, can damage the eye's surface tissues as well as the cornea and lens, Unfortunately, many people are unaware of the dangers UV light can pose. By wearing UV-blocking sunglasses, you can enjoy the summer safely while lowering your risk for potentially blinding eye diseases and tumors."

## Ultraviolet (UV) light can harm your eyes

Too much exposure to UV light raises your risk of eye diseases and other problems. Here are a few of the eye conditions you can avoid by wearing sunglasses:

  - Cataracts and eye cancers can take years to develop. Each time you bask in the sun without eye protection, you increase your risk of serious disease. Babies and children need to wear hats and sunglasses for this very reason. People of all ages should take precautions whenever they are outdoors.

  - Growths on the eye, such as pterygium, can show up in our teens or 20s. Surfers, skiers, fishermen, farmers and others who spend long hours under the midday sun or near rivers, oceans and mountains are at risk.

  - Snow blindness, a form of **photokeratitis,** can quickly develop after exposure to UV reflections off of snow, ice, sand or water.

![](/images/sunglasses-hat.jpg)

### Protect your eyes from sun damage in every season

Many sunglasses shoppers forget to check the UV rating before purchasing a pair. Be sure to select sunglasses that provide 100% UV or UV400 protection, or block both UV-A and UV-B rays. If you're unsure, check out our recommended types of sunglasses.

Here are additional tips to protect your eyes from UV damage, no matter what the season: 

  - Wear a hat along with your sunglasses. Broad-brimmed hats are best.
  - Protect children and senior citizens with hats and sunglasses. Everyone is at risk for sun damage.
  - Know that clouds don't block UV light. The sun's rays can pass through haze and clouds.
  - Sunlight is strongest midday to early afternoon, at higher altitudes and when reflected off of water, ice or snow.
  - Never look directly at the sun. Doing so at any time, including during an eclipse, can damage the eye's retina and cause a serious injury known as solar retinopathy.
  - Avoid tanning beds. Tanning beds pose the same risks to your eyes and body as outdoor UV light.

By embracing these simple guidelines, you and your family can enjoy the sun safely all year long.

## Sunlight and your health

Healthy exposure to sunlight can have positive effects, as long as you protect your eyes from UV damage.

You need a little natural light every day to help you sleep well. That's because the light-sensitive cells in our eyes play an important role in our body's natural wake-sleep cycles. This is especially important as we age and become more apt to insomnia.

Spending time outdoors in the daylight can also help prevent nearsightedness in kids. Not only is exercise great for eye health, but exercising outside may be additionally beneficial. Take your children outside to play to help lower their risk for nearsightedness and teach them good habits for a lifetime of eye health.

Just don’t forget those hats and sunglasses!

### Regular Eye Exams
Make sure you schedule a comprehensive eye exam on a regular basis to ensure your eyes are healthy.  If you are over 50 or have increased risk factors for eye disease, you should schedule exams at least on a yearly basis or according to your eye doctor’s recommendations. 