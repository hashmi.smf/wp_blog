---
title: Know How and When to Treat an Eye Infection
author: Westpoint Optical
tags:
 - eye infection
 - eye drops for eye infection
 - home remedies for eye infection
 - eye infection treatment
 - eye infection in kids
 - eye infection symptoms
 - eye infection drops
 - baby eye infection home remedy
 - how to cure eye infection in 24 hours
 - home remedy for eye infection
 - eye infection types
 - red eye infection
 - eye infection medicine
 - eye infection home remedy
 - herpes eye infection
 - baby eye infection
 - eye infection remedies
 - bacterial eye infection
 - antibiotics for eye infection
 - eye drop for eye infection

Categories: Know How and When to Treat an Eye Infection
date: 2021-04-14 13:25:51
---

![Eye Infection](/images/Know-How-and-When-to-Treat-an-Eye-Infection.jpg)

In the event that you've seen some agony, growing, tingling or redness in your eye, you probably have an eye disease. Eye diseases fall into three explicit classifications dependent on their motivation: viral, bacterial, or contagious, and each is dealt with in an unexpected way. 

The uplifting news is eye diseases aren't difficult to spot, so you can look for treatment rapidly. 

Here are the beginning and end you need to think about the eight most basic eye diseases so you can sort out the reason and what to do about it.

## 1. Conjunctivitis/pink eye
Irresistible conjunctivitis, or pink eye, is perhaps the most widely recognized eye disease. It happens when veins in the conjunctiva, the meager peripheral layer encompassing your eyeball, become tainted by microorganisms or an infection. 

Subsequently, your eyes become pink or red and aroused. 

It can likewise result from sensitivities or openness to synthetics, similar to chlorine, in pools. 

Conjunctivitis brought about by microbes or infections is incredibly infectious. You can in any case spread it as long as about fourteen days after the disease begins. Observe any of the accompanying side effects and consider your to be as quickly as time permits for treatment:

 - reddish or pinkish tint to your eyes
 - watery discharge from your eyes that’s thickest when you wake up
 - itchiness or feeling like there’s something constantly in your eyes
 - producing more tears than usual, especially in only one eye

You’ll likely need the following treatments depending on which type of conjunctivitis you have:

 - **Bacterial:** Antibiotic eye drops, ointments, or oral medications to help kill bacteria in your eyes. After starting antibiotics, symptoms fade in a couple of days.

 - **Viral:** No treatment exists. Symptoms tend to fade after 7 to 10 days. Apply a clean, warm, wet cloth to your eyes to relieve discomfort, wash hands frequently, and avoid contact with others.
 - **Allergic:** Over-the-counter (OTC) antihistamines like diphenhydramine (Benadryl) or loratadine (Claritin) help relieve allergy symptoms. Antihistamines can be taken as eye drops, and anti-inflammatory eye drops can also help with symptoms.

## 2. Keratitis
Irresistible keratitis happens when your cornea gets contaminated. The cornea is the reasonable layer that covers your understudy and iris. Keratitis results from either disease (bacterial, viral, contagious, or parasitic) or an eye injury. Keratitis implies growing of the cornea and isn't generally irresistible.

Symptoms of keratitis can include:

 - edness and swelling in your eye
 - eye pain or discomfort
 - producing more tears than usual or an abnormal discharge
 - pain or discomfort when you open and close your eyelids
 - loss of some vision or blurry vision
 - light sensitivity
 - sensation of having something stuck in your eye

## 3. Endophthalmitis
Endophthalmitis is a serious aggravation within your eye coming about because of a bacterial or parasitic disease. Candida parasitic diseases are the most well-known reason for endophthalmitis. 

This condition can occur after certain eye medical procedures, like waterfall medical procedures, albeit this is uncommon. It might likewise occur after your eye is entered by an article. A few manifestations to keep an eye out for, particularly after a medical procedure or an eye injury, include:

 - mild to severe eye pain
 - partial or complete vision loss
 - blurry vision
 - redness or swelling around the eye and eyelids
 - eye pus or discharge
 - sensitivity to bright lights

## 4. Blepharitis
Blepharitis is an irritation of your eyelids, the skin folds covering your eyes. This sort of irritation is typically brought about by obstructing the oil organs inside the eyelid skin at the base of your eyelashes. Blepharitis might be brought about by microorganisms.

Symptoms of blepharitis include:

 - eye or eyelid redness, itchiness, swelling
 - eyelid oiliness
 - sensation of burning in your eyes
 - feeling like something’s stuck in your eyes
 - sensitivity to light
 - producing more tears than usual
 - crustiness on your eyelashes or corners of your eyes

## 5. Sty
A pen (additionally called a hordeolum) is a pimple-like knock that creates from an oil organ on the external edges of your eyelids. These organs can get obstructed with dead skin, oils, and other matter and permit microorganisms to congest your organ. The subsequent disease causes a pen.

Sty symptoms include:

 - pain or tenderness
 - itchiness or irritation
 - swelling
 - producing more tears than usual
 - crustiness around your eyelids
 - increased tear production

## 6. Uveitis
Uveitis happens when your uvea gets inflamed from infection. The uvea is the central layer of your eyeball that transports blood to your retina — the part of your eye that transmits images to your brain.

Uveitis symptoms can include:

 - eye redness
 - pain
 - “floaters” in your visual field
 - sensitivity to light
 - blurry vision 

## 7. Cellulitis
Eyelid cellulitis, or periorbital cellulitis, happens when eye tissues get infected. It’s often caused by an injury like a scratch to your eye tissues that introduces infectious bacteria, such as Staphylococcus (staph), or from bacterial infections of nearby structures, such as sinus infections.

Cellulitis symptoms include eyelid redness and swelling as well as eye skin swelling. You typically won’t have any eye pain or discomfort.

## 8. Ocular herpes
Ocular herpes happens when your eye is infected by the herpes simplex virus (HSV-1). It’s often just called eye herpes.

Eye herpes is spread by contact with someone who has an active HSV-1 infection, not through sexual contact (that’s HSV-2). Symptoms tend to infect one eye at a time, and include:

 - eye pain and irritation of the eye
 - sensitivity to light
 - blurry vision
 - eye tissue or corneal tears
 - thick, watery discharge
 - eyelid inflammation
