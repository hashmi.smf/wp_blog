---
title: Eye Exams Are Important Even With 20/20 Vision
author: Westpoint Optical
tags: 
  - eye exam near me	
  - eye doctor
  - newlook
  - eye test
  - eye exam cost
  - ottawa eye exam
  - eye clinic
  - optometrist tools	
  - free eye exam
  - online eye exam
  - how long does an eye exam take	
  - eye exam in brampton	
  - human eye
  - eyes
  - womens eye
  - mens eye
  - how many frames per second can the human eye see
  - how many fps can the human eye see
  - human vision
  - eye facts
  - eye lens function
  - sun with sunglasses
  - sun glasses
  - eye care
  - westpoint optical stores
  - optical in brampton
  - eye test near me
  - westpoint eyewear

Categories: Eye Exams Are Important Even With 20/20 Vision
date: 2021-03-31 16:44:52
---

## 20 Facts on 20/20 Vision

![](/images/eye-exam.jpg)

You have probably heard the term “20/20 vision” a number of times. If you have ever tested your vision, you must remember that the doctor made you read a chart with some letters printed on them.

If you were told that you have a 20/20 vision that means, your visual acuity is normal. In other words, your eyesight is perfect!

But, is this true? Does a 20/20 vision mean that you have a perfect vision?
Want to know? Check these 20 surprising facts on 20/20 vision:

**1.**    20/20 is just a measurement of how clearly you are able to see at a particular distance.  If your visual acuity report says that you have a 20/20 vision then that means you can clearly see objects at a distance of 20 feet 1. If you have a 20/40 vision then that means you need to stand at 20 feet to see anything that a person with normal vision can see from 40 feet. This determines your visual clarity under standard circumstances and doesn’t determine the quality of your vision under any type of circumstances.

**2**.    Don’t think that 20/20 vision is the perfect vision. You may even have a 20/15 vision and can see sharper than average images.  A 20/15 vision means that you can read the eye chart standing at 20 feet while any other person needs to stand at a distance of 15 feet to easily see the chart. Usually, vision correction glasses and processes aim at giving you a 20/20 vision. A special eye chart is used for measuring your visual acuity or the clarity of your vision.

**3.**    Generally, doctors use the Snellen chart for measuring your visual acuity. The chart contains a total of 11 rows of capital letters. The topmost line of the chart contains a single but very large letter. In the lines following the first line, the number-letter keeps increasing and the size of the letter keeps decreasing. The doctor will make you stand at a distance of 20 feet from the chart and read all the letters without contact lenses or glasses.

You may have to cover one eye and then read the smallest line of letters that are visible to you. The doctor will perform the test in both of your eyes. In some cases, the doctor may make you read the chart using a mirror. In this case, the test can also be done from a distance that’s less than 20 feet. The Snellen Chart was developed by Dr Hermann Snellen, a Dutch Doctor, in the 1860s. Hermann Snellen was the colleague of Dr Fransiscus Donders. It was Donders who first started to diagnose vision-related issues in people by making them look at a chart and ask what they can see.

**4.**    There is nothing called a “perfect vision.” Vision might defer depending on the circumstances. For instance, if you are out on the road on a sunny day, you may enjoy a clear vision but your friend who doesn’t have a 20/20 vision and has low visual clarity compared to you, may too enjoy a clear vision under the same circumstance just because he or she is wearing a  UV protected polarised glasses that has anti-reflective coating for that improved the contrast and also blocks glare.

   There is a difference between visual acuity, vision, eyesight. Before we proceed further, let’s understand the difference:

**5.**    Visual acuity refers to the sharpness of your vision. This is measured by your ability to read the letter in the Snellen Chart from a particular viewing distance as prescribed.

**6.**    Visual acuity is an invariable measurement. It’s because you are either sitting or standing during the test and the objects (the letters and the chart in this case) that you are viewing are also static.

**7.**    The doctor also tests your visual acuity under high contrast conditions. In such case, the letters on the chart are written in black and the background is white.

**8.**    Visual acuity test helps in determining your eye clarity under standard conditions. This doesn’t determine the quality of your vision under various circumstances. For instance, this test cannot predict how well you may see coloured and moving objects and objects that have similar brightness levels as their background.

**9.**    There are certain neurological and physical factors that determine your visual acuity. These are:
•    How well your eye lens and cornea can focus light on the retina.
•     The sensitivity of the nerves in your retina and the visual centres in the brain.
•    How your brain interprets the information that it received from the eyes

The light that is focused on the macula (the central part of the retina) determines your visual acuity results during the eye examination.

**10**.    Eyesight can be defined in many ways. It can be referred to as your vision or the ability to see or your range of sight. Often the term “eyesight” is used interchangeably with “visual acuity.”

**11.**    Vision is a much wider term than eyesight and visual acuity. Vision is not just seeing but also includes a number of your visual abilities such as tracking moving objects, your depth perception, contrast sensitivity, focusing speed and accuracy.

**12.**    Often, your 20/20 vision may not be sharp enough. This means that your eyes may have Higher-Order Aberrations or HOA. This cannot be corrected using contact lenses or vision correcting eyeglasses. The eye doctor may use wavefront technology for checking the aberrations.

In case the aberrations are caused by minor irregularities occurring on the front surface of your eye, then it can be corrected by fitting Gas Permeable (GP) contact lenses. This often improves the visual acuity better than soft contact lenses and glasses. It’s because the GP lenses are rigid. These lenses replace the irregular front area of your eye, making them perfectly smooth and curved surface that can focus light more accurately.

**13.**    Custom wavefront LASIK is another option. This is a laser vision correction surgery can provide you sharp vision (that you get with the rigid gas permeable contact lenses). The visual acuity achieved by LASIK eliminates the daily hassle of wearing glasses or contact lens care.

**What can you do to keep your eyes healthy?**

Here is the guide to maintain a 20/20 vision naturally:

**14.    Eat healthily**
Eating well and maintaining a healthy weight can benefit your eyes tremendously. There are a number of food items that can protect your eyes against a number of eye conditions such as age-related macular degeneration, and cataracts.

These are caused due to the lack of certain nutrients known as zeaxanthin and lutein found in a variety of vegetables and fruits such as spinach, broccoli, mango, and green beans. Eating fresh and seasonal vegetables, dairy products, fish (especially cold water fish rich in Omega-3 fats) save you from vision impairment.

**15.    Avoid smoking**
It’s because smoking restricts the flow of blood to your eyes thus, decreasing the amount of oxygen in your eyes. This causes oxidative stress to your eyes and can damage your retina causing the death of cells in that area and a number of other visually impairing eye conditions.

**16.    Never ignore any vision-related symptoms**
Whenever you notice any changes in your vision, get in touch with your eye specialist. Ignoring these symptoms (howsoever minor it may be) is the biggest mistakes that you will be making. Often, minor symptoms may trigger major issues and cause progressive vision loss.

People usually ignore these symptoms thinking of them as a minor issue. And when they realise, the harm is already done. So, never ignore any symptoms. Get in touch with your eye care specialist today and get a comprehensive eye examination done.

Also, always wear quality sunglasses whenever going out in the sun. Be careful when selecting a sunglass. It’s because not all sunglasses can protect you from the harmful UV rays that damage your eyes. Ensure that the sunglass that you are buying blocks at least 95% to 99% UV rays (if it’s 100% then better).

**17.    Let your eyes rest for a while in between work**

Overstressing your eyes can harmful. So, whenever you are working in front of your computer for long periods, give them a break in between your work. Take frequent screen breaks and look to any distant object for a brief period.

You may try the 20-20-20 rule. Try looking at an object located at a distance of 20 feet for 20 seconds. Do this every 20 minutes while you work. This relaxes the eye muscles and adjusts their focusing ability.

Eye strain is mainly caused by muscle strain and when your eyes dry out.  Try setting up your computer at the corner of your room. You may try setting it up near a window. You may look out of the window. This not only refreshes your mind but also keeps your eyesight healthy. Also, keep your hands clean. Knowingly or unknowingly you are always touching your eyes. This increases the risk of eye infection. This is particularly true when you are wearing contact lenses.

Foods for a healthy eyesight and 20/20 Vision
Include these food items in your diet for a 20/20 vision or healthy vision:

**18.    Kale and Spinach:** Dark green leafy vegetables such as Spinach and Kale are excellent for your eyesight. These contain high amounts of antioxidants such as lutein and zeaxanthin. These two nutrients are found in your eyes and help in lowering the risk of cataracts and age-related macular degeneration.

People who eat a high amount of lutein and zeaxanthin are less likely to develop eye conditions such as cataracts. If you do not like eating Kale and Spinach then you may eat other leafy veggies such as Collards, Romaine Lettuce, and Turnip Greens.

These also contain high amounts of Zeaxanthin and Lutein. Other vegetables that contain these two anti-oxidants include corn, peas, and broccoli. Also, eggs are a good source of Lutein and Zeaxanthin.

**19.    Black-eyed peas and Legumes:** Any and all kind of legumes are good for your eyes. Be it black-eyed peas, lima beans, and kidney beans- these contain high levels of zinc. This is an essential trace mineral that is also found in your eyes. Zinc helps in safeguarding your eyes from the light. Other foods items that contain high levels of zinc include lean red meat, oysters, fortified cereals, and poultry.

**20.    Salmon:** A diet rich in Omega-3 fats is extremely good for your eyes. Coldwater fishes such as Sardine, Salmon, Tuna, and Halibut are rich in Omega-3 fats. These help in preventing the risk of eye issues later in your life. People who eat a diet high in omega-3 fatty acid are less likely to develop Age-related Macular Degeneration than people who don’t.

Also, eat plenty of citrus fruits such as tangerines, oranges, lemons, and grapefruit. These contain high levels of vitamin C, an antioxidant that’s crucial for your eye health. Your eyes need a steady supply of Vitamin C for functioning properly and high levels of antioxidants for preventing or delaying the development of AMD and cataract. Other food items containing Vitamin C include tomatoes, strawberries, peaches, red peppers, and oranges.

**Get in touch with the Experts**
If you think you have refractive errors, it’s better to get in touch with the eye specialists for evaluation of your vision. If you have a minor refractive error, you may undergo LASIK Eye Surgery in Canada to improve your visual acuity.

Vision errors such as nearsightedness, farsightedness, and astigmatism can be corrected with glasses and contact lenses. However, if you want to get rid of the glasses and do not enjoy wearing a contact lens, you can undergo a LASIK eye surgery.

LASIK eye surgery doesn’t cost much and is a matter of only a few minutes. If you want more information on LASIK or want to undergo LASIK eye surgery in Canada, get in touch with the experts at Westpoint Optical. Contact us today [CLICK HERE APPOINTMENT]( https://westpointoptical.ca)





