---
title: The Right and Wrong Way to Clean Your Glasses
author: Westpoint Optical
tags:
  - best way to clean glasses
  - eye glass cleaner	
  - how to clean eyeglasses	
  - cleaning glasses	
  - clean glasses	
  - how to clean glasses nose pads	
  - how to properly clean glasses	
  - best way to clean eyeglasses	
  - how to get super glue off glass	

Categories: The Right and Wrong Way to Clean Your Glasses
date: 2021-03-31 19:17:32
---

# How To Clean Glasses the Right Way

![](/images/cleaning-lenses-hero.jpg)

Cleaning your eyeglasses daily is the best way to keep them looking great and prevent lens scratches and other eyewear damage.

But there's a right way — and plenty of wrong ways — when it comes to how to clean glasses.

## How to clean your glasses
Follow these tips to clean your eyeglass lenses and frames without risk of scratching the lenses or causing other damage. These cleaning tips will help you keep your **sunglasses**, **safety glasses** and **sports eyewear** in great shape, too.

### 1. Wash and dry your hands thoroughly.
Before cleaning your eyeglasses, make sure your hands are free from dirt, grime, lotion and anything else that could be transferred to your lenses. Use lotion-free soap or dishwashing liquid and a clean, lint-free towel to clean your hands.

### 2. Rinse your glasses under a gentle stream of lukewarm tap water.
This will remove dust and other debris, which can help avoid scratching your lenses when you are cleaning them. Avoid hot water, which can damage some eyeglass lens coatings.

### 3. Apply a small drop of lotion-free dishwashing liquid to each lens.
Most dishwashing liquids are very concentrated, so use only a tiny amount. Or apply a drop or two to your fingertip instead. Use only brands that do not include lotions.

### 4. Gently rub both sides of the lenses and all parts of the frame for a few seconds.
Make sure you clean every part, including the nose pads and the ends of the temples that rest behind your ears. Also, clean the area where the edge of the lenses meet the frame — dust, debris and skin oils frequently accumulate here.

### 5. Rinse both sides of the lenses and the frame thoroughly.
Failing to remove all traces of soap will cause the lenses to be smeared when you dry them.

### 6. Gently shake the glasses to eliminate most of the water from the lenses.
Inspect the lenses carefully to make sure they are clean.

### 7. Carefully dry the lenses and frame with a clean, lint-free towel.
Use a dish towel that has not been laundered with a fabric softener or dryer sheet (these substances can smear the lenses). A cotton towel that you use to clean fine glassware is a good choice. Make sure the towel is perfectly clean. Dirt or debris trapped in the fibers of a towel can scratch your lenses; and cooking oil, skin oil or lotion in the towel will smear them.

### 8. Inspect the lenses again.
If any streaks or smudges remain, remove them with a clean microfiber cloth — these lint-free cloths are available at most optical shops or photography stores.

For touch-up cleaning of your glasses when you don't have the above supplies available, try individually packaged, pre-moistened disposable lens cleaning wipes. These are formulated specifically for use on eyeglass lenses.

## Which brings us to a very important topic — what NOT to use to clean your glasses.

### CLEANING GLASSES - DON'TS

**DON'T** use your shirttail or other cloth to clean your glasses, especially when the lenses are dry. This can scratch your lenses.

**DON'T** use saliva to wet your lenses.

**DON'T** use household glass or surface cleaners to clean your eyeglasses. These products have ingredients that can damage eyeglass lenses and coatings, such as anti-reflective coating.

**DON'T** use paper towels, napkins, tissues or toilet paper to clean your lenses. These can scratch or smear your lenses or leave them full of lint.

**DON'T** try to "buff away" a scratch in your lenses. This only makes the situation worse.

If tap water isn't available to rinse your lenses before cleaning them, use plenty of the spray eyeglass cleaner to flush away dust and other debris before wiping the lenses dry.

## Eyeglass cleaners and cleaning cloths

Spray eyeglass lens cleaners are available where you purchase your glasses or at your local drug or discount store. These can be helpful if you are traveling or don't have dishwashing soap and clean tap water available.

If tap water isn't available to rinse your lenses before cleaning them, use plenty of the spray eyeglass cleaner to flush away dust and other debris before wiping the lenses dry.

If your lenses have **anti-reflective (AR)** coating, make sure the eyeglass cleaner you choose is approved for use on anti-reflective lenses.

When using individually packaged, pre-moistened disposable lens cleaning wipes, first inspect the lenses for dust or debris. To avoid scratches, blow any debris off the lenses before wiping them.

Microfiber cleaning cloths are an excellent choice for cleaning glasses. These cloths dry the lenses very effectively and trap oils to avoid smearing.

But because they trap debris so effectively, make sure you clean the cloths frequently. Hand-wash the cloth using lotion-free dishwashing liquid and clean water; allow the cloth to air dry.

## How to remove scratches from glasses

Unfortunately, there is no magic cure for scratched lenses. Once your glasses are scratched, they are scratched.

Some products are designed to make the scratches look a little less visible — but these are essentially waxy substances that wear off easily, and results are mixed, depending on the location and depth of the scratches. Also, these products often will smear lenses that have AR coating.

In addition to reflecting light and interfering with vision, scratches can affect the impact resistance of the lenses. For optimum vision and safety, the best thing to do if you notice significant scratches is to purchase new lenses.

When purchasing eyeglass lenses, choose lenses that have a durable scratch-resistant coating. And ask your optician if your purchase includes an anti-scratch warranty — especially if scratched lenses have been an issue in the past.

## When to have your glasses cleaned professionally

If your lenses are in good shape but the nose pads or other components of the frame have become impossible to keep clean, see your eye doctor or the person that sold the eyeglasses to you.

Sometimes, eyeglasses can be cleaned more thoroughly with an ultrasonic cleaning device, and yellowing nose pads can be replaced with new ones. But don't try these fixes at home — see a professional.

## Use a protective storage case

Eyeglass lenses can easily get scratched if you fail to store them somewhere safe. This includes when you take them off at bedtime.

Always store your eyeglasses in a clean storage case, and NEVER place them on a table or counter with the lenses facing down.

If you don't have a glasses case handy, place your glasses upside down with the temples open — somewhere safe, where they won't get knocked off a table or countertop.

## Glasses don't last forever

All eyeglass lenses will get a few scratches over time from normal use and exposure to the environment — and from occasionally getting dropped or misplaced. Eyeglasses lenses are scratch resistant, not scratch-proof.

When purchasing glasses, ask your eye doctor or eyewear salesperson about anti-scratch warranties for your lenses. This is especially important for children's eyeglasses or if you wear glasses in dusty conditions.

Following the above tips is the best way to keep your glasses clean and scratch-free for as long as possible.

If your lenses are badly scratched and your eyeglass prescription has expired — or you simply want new glasses — schedule an eye exam.

**NEED AN EYE EXAM?** Find an [eye doctor near you](https://westpointoptical.ca/contact.html) and make an appointment.

