---
title: The Dangers Of Decorative Contact Lenses
author: Westpoint Optical
tags:
  - contact lenses
  - eye lens
  - contact lenses price
  - coloured contact lenses
  - types of lenses
  - how to wear contact 
  - concave lens
  - bausch and lomb
  - eye contact
  - optical stores near me
  - eye doctor
  - eye infection
  - clear vision
  - eye drops
  - contact lenses canada
  - vision problems

Categories: The Dangers Of Decorative Contact Lenses
date: 2021-04-03 17:19:07
---

From athletes to pop stars and Halloween costumes to fashion accessories, decorative contact lenses have caught the eye of many people. Not surprisingly, decorative contact lens sales increase significantly at Halloween as many look for that unique detail to enhance a costume. Unfortunately, ophthalmologists say they also see a significant increase in eye injuries to accompany increased wear of decorative lenses.

![](/images/risks-of-contact-lenses.jpg)

## What types of eye problems can wearing decorative lenses cause?

According to eyeSmart, one study reported **13,500 emergency room visits per year related to teenagers and children wearing decorative lenses.** Those injuries include:
  - Painful bacterial infections (keratitis)
  - Scratched corneas
  - Cuts & open sores on the iris
  - Permanent vision loss

In fact, **16 times more costume contact lens wearers experience a severe bacterial infection** of the eye than do individuals wearing regular, corrective lenses. Also, **60% of those who do experience bacterial infection from decorative lenses also suffer permanent vision loss!**

## Why are decorative lenses such a problem?

Decorative lenses are often purchased online and in novelty stores, which means they can come with hygiene problems from the start, are often worn by those who don’t know how to care for contact lenses, and can fail to provide a proper fit.

One type of decorative lenses known as circle lenses is illegal in the Canada. Circle lenses are larger than other contact lenses and extend into the whites of the eyes. They are often worn by those who want to achieve a “doe-like look” similar to that featured in a favorite pop song video. (Note: The look was digitally enhanced, not contacts.)

One woman, Laura Butler, discovered the danger of decorative contact lenses when she purchased a pair in a novelty shop for $30 and ended up paying $2,000 in medical bills in addition to experiencing severe eye pain and infection and possibly permanent partial vision loss. Butler later discovered she could have purchased two sets of decorative lenses from her optometrist for $50 plus $60 for the eye exam to achieve the same look.

## Do decorative lenses enhance vision?

While much of their use purely involves visual appeal, some professional athletes now use colored contact lenses because they say it improves their athletic performance. Examples include Brian Roberts (former professional baseball second baseman) who wore tinted contacts during day games to improve visibility and A.J. Pierzynski (catcher for the Atlanta Braves) who wears them instead of sunglasses, which can slip, get sweaty and add unwanted bulk.

While research has yet to prove that colored contacts do improve performance, Business Insider reports that many athletes in a variety of sports believe they do enhance performance.

### How can I safely wear decorative contact lenses?

To prevent permanent eye damage and possibly blindness, heed the following advice when purchasing decorative contact lenses:

**1. Beware of lawbreakers.** The FDA regulates all contact lenses, whether they’re corrective or not. Any vendor who doesn’t require a prescription or your doctor’s contact information is breaking the law. Only buy contacts from a company with FDA clearance to sell contact lenses. The Canada has made it illegal to purchase circle lenses.

**2. Consult an expert**. An eye doctor will ensure your contacts fit properly and teach proper contact care. Plus, he can diagnose and prevent significant eye disease and damage. Also, utilize follow-up visits to ensure lenses aren’t causing any irritation. Discuss any discomfort, even if it seems minor since damage can be cumulative.

**3. Practice proper lens care.** Read and follow the instructions that accompany contacts as well as those given by the doctor. Taking care of contacts results in preventing significant inconvenience, expensive doctor bills, and even permanent damage.

**4. Be selfish.** When it comes to contact lenses, don’t share. They are not one-size-fits-all, and infection can transfer from one person’s eyes to another’s via contacts. Besides, every individual requires a different fit, and ill-fitting contacts can cause eye damage over time.

**5. Use your prescription.** Getting a prescription from an eye doctor isn’t enough; you have actually to use it when purchasing contacts. Quality contact retailers will not only ask for this prescription but your eye doctor’s name and phone number as well.

Also, know the signs of eye damage, which include redness, pain that doesn’t go away and decreased vision. Remove contacts, and see an eye doctor immediately if you notice any of these signs of eye damage.

Having eyes like a cheetah or your favorite rock star may seem fun, but purchasing from an unauthorized seller is dangerous. Leaving your eye doctor out of the mix can easily result in an agonizing recovery process, partial vision loss, and even blindness!

Be smart about what you put into your eyes to help ensure your good eye health for a lifetime.

