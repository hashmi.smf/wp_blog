---
title: Why Are Your Eyes Sensitive To Light?
author: Westpoint Optical
tags:
  - light sensitivity	
  - photophobia	
  - light sensitivity causes	
  - sensitivity to light	
  - sudden light sensitivity	
  - fluorescent light headaches	
  - eyes sensitive to light	
  - photosensitivity eyes	
  - blue eyes in the sun	
  - eyes sensitive to light	
  - sensitivity to light and sound	
  - eye exam near me
  - westpoint optical 
  - sunlight
  - eye facts
  - eye lens function
  - sun with sunglasses
  - sun glasses
  - eye care
  - westpoint optical stores
  - optical in brampton
  - eye test near me
  - westpoint eyewear

Categories: Why Are Your Eyes Sensitive To Light?
date: 2021-03-31 18:32:18
---

## PHOTOPHOBIA: WHY ARE YOUR EYES SENSITIVE TO LIGHT?

Light sensitivity, also called photophobia, is a common condition that leads people to experience discomfort or eye pain caused by light exposure. The fact that people experience light sensitivity at all is interesting, considering the role light plays in our ability to see: light is at the center of a multi-step process that involves many different parts of the eye.

Before we look at the causes of photophobia and tips for living with light sensitivity, let’s start by examining how light is used by your eyes to see. This will set the stage for further discussions about causes and treatment of light sensitivity.

  - Light reflects off objects in your vicinity and enters the front of your eye
  - Inside the eye, light first encounters a thin veil of tears
  - The cornea is behind the tear veil, and its job is to focus the light toward the back of the eye
  - The light passes through the aqueous humor, then the pupil
  - The pupil adjusts in size to control the amount of light that passes through
  - The lens focuses the light and adjusts the shape depending on whether the object is near or far
  - By this time, the light has reached the center of the eye, which contains a gel-like substance called the vitreous
  - The retina and its photoreceptors at the back of the eye send light through nerve fibers to the optic nerve
  - Finally, the light signal arrives at the visual center of the brain, where we process the “images” we see
This process is even more impressive when you consider how fast it happens in real time—like how eyes can register 90+ MPH fastballs in milliseconds. Because there are so many steps to the process, different areas of the eye could be the cause of light sensitivity and the photophobia could be a symptom of another health condition.

## What Causes Photophobia?

According to Medline Plus from the National Library of Medicine, some causes of sensitivity to light include:

  - Eye inflammation
  - Corneal abrasion or ulcer
  - Wearing contact lenses too long or lenses that fit poorly
  - Eye infection, diseases, injury or recovering from eye surgery
  - Meningitis
  - Migraine headaches
  - Medications

### Light sensitivity caused by headaches and brain conditions

Up to 80 percent of people who have photophobia experience a migraine along with the sensitivity to light. According to the American Migraine Foundation, light sensitivity is a symptom of migraines. Not all headaches that cause light sensitivity are migraines—individuals who have tension and cluster headaches can also have sensitivity around bright lights.

If you experience chronic headaches due to light sensitivity, discuss your symptoms with a doctor as these may indicate a serious condition, including a brain injury or disease.

Tumors in the pituitary gland can also contribute to eye pain and headache issues. In some cases, the tumor produces hormones that can cause a variety of symptoms, including headache. In other cases, tumors can grow and begin putting pressure on the surrounding areas in the brain and nerves and can result in headaches and vision loss. Treatment depends on the symptoms; your doctor may suggest surgery or medicines to counteract excess hormones.

### Medicines that may cause light sensitivity

Some medications and supplements contain ingredients that may increase sensitivity to light, according to the Wisconsin Department of Health Services. In addition to photophobia, these drugs may induce a change in the skin that can cause an exaggerated skin burn, itching, scaling, rash, or swelling with exposure to ultraviolet light.

Some photosensitizing medications include:

  - Antihistamines
  - Furosemide
  - Non-steroidal, anti-Inflammatory drugs (NSAIDs)
  - Oral and estrogen-based contraceptives
  - Quinine
  - Sulfonamides
  - Tetracycline
  - Tricyclic anti-depressants

### Light sensitivity due to eye diseases and conditions
The American Academy of Ophthalmology says some eye diseases and conditions can cause photophobia, including:

  - Cataracts
  - Corneal abrasion
  - Conjunctivitis or pink eye
  - Dry eye
  - Eye allergies
  - Keratitis which is also caused corneal inflammation
  - Uveitis, an inflammation of the eye

Because increased sensitivity to light is a symptom of many eye conditions, you should schedule an exam with your eye doctor as soon as you notice a marked increase in photophobia.

## Living with Light Sensitivity

![](/images/living-with-light-sensitivity.jpg)

Managing light sensitivity can be a matter of making lifestyle changes to protect your eyes from sunlight. One suggestion is to wait for a moment when transitioning from areas that have different levels of light. This helps the eyes adjust to the change and could lessen discomfort.

Other ways to minimize sensitivity include:

  - Shielding your eyes from the sun while outdoors
  - Wear polarized sunglass lenses to help cut down the sun’s glare
  - All sunglass lenses should be UV-protected to protect eyes from harmful UV-light
  - Or, choose light-activated tinted glasses to help reduce sunlight’s impact
  - Wrap-around sunglasses may also prevent light from getting in through the sides

If your doctor has diagnosed photophobia due to dry eyes, try to keep eye drops on hand to minimize discomfort. However, if there are other underlying causes, such as cataracts or inflammation, your eye doctor may have other lifestyle recommendations.

### Treatment for Photophobia

Addressing the underlying causes of eye sensitivity is the best way to treat the condition. Whether light sensitivity accompanies migraines or whether it’s caused by an injury, disease or medication, your eye doctor is your best resource to minimize sensitivity to light.

If you need a qualified eye doctor to help you treat photophobia or manage the health of your eyes, contact the doctors at Westpoint Optical Eye Care. Take the next step toward better vision—schedule your eye exam today, get in touch with the experts at Westpoint Optical. Contact us today [CLICK HERE FOR APPOINTMENT]( https://westpointoptical.ca)