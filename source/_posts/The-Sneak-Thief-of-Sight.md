---
title: The Sneak Thief of Sight
author: Westpoint Optical
tags:
  - sun with sunglasses
  - sun glasses
  - eye care
  - westpoint optical stores
  - optical in brampton
  - eye test near me
  - westpoint eyewear
  - eye exam near me
  - contact lenses
  - kids eye wear
  - eye doctor
  - optical near me
  - contact lenses for kids
  - contact lens
  - contact lens case
  - eye sight
  - eye health 
  - westpoint optical eye wear
  - contact focal point
  - eye sight
  - mental health
  - eye health
  - eye exercise
  - eye doctor
Categories: The Sneak Thief of Sight
date: 2021-05-08 14:34:53
---
![Thief of Sight](/images/The-Sneak-Thief-of-Sight.jpg)
It's that season once more. January is National Glaucoma Awareness Month, a period put away every year to make mindfulness about this conceivably destroying infection. The explanation mindfulness about glaucoma is so significant is on the grounds that as its epithet, The Sneak Thief of Sight, depicts, the infection frequently makes perpetual harm your eyes and vision with no recognizable manifestations, until it's past the point of no return. Truth be told, up to 40% of your vision could be lost with no observable manifestations! This is the reason mindfulness and early recognition are fundamental for powerful treatment.

### What is Glaucoma?
Glaucoma is the main source of lasting visual impairment around the world. It is a gathering of eye illnesses that outcomes in harm to the optic nerve, which can prompt vision misfortune and possible visual impairment. 

Most instances of glaucoma happen without clear manifestations. Frequently individuals figure they will encounter migraine or eye torment, be that as it may, this is generally a misinterpretation. There are a few sorts of glaucoma and just one, point conclusion glaucoma, normally gives torment.

### Treatment for Glaucoma
While there is still no remedy for glaucoma, there are prescriptions and surgeries that can forestall and moderate any further vision misfortune. Nonetheless, any vision that is lost is irreversible, generally. Once more, this is the reason early location is critical to halting and forestalling vision misfortune and visual impairment. Glaucoma screening incorporates various tests. Numerous individuals accept the "air-puff" test used to gauge eye pressure is the thing that recognizes glaucoma, yet this isn't the entire picture. Truth be told, numerous individuals can create glaucoma with ordinary eye pressure. Today fresher advances are accessible, like OCT (like an ultrasound), which permits eye specialists to gaze straight toward the optic nerve to survey glaucoma movement. The treatment plan relies upon various elements including the kind of glaucoma and seriousness of the eye harm. 

While anybody can be influenced by glaucoma, there are sure danger factors that are known to improve the probability of getting the sickness. Monitoring the danger factors and knowing whether you are at higher danger places you in a superior situation to move toward avoidance, including standard screenings by an eye specialist.Here are some of the major risk factors:

### Glaucoma Risk Factors
 - Over 60 years old (over 40 for African Americans)
 - Family history of glaucoma 
 - African or Hispanic descent
 - Previous eye injury or surgery - even a childhood eye injury can lead to glaucoma decades later
 - Diabetes
 - High nearsightedness or farsightedness
 - Cortisone steroid use (in the form of eye drops, pills, creams etc.) 

### Glaucoma Prevention
Now that you know the risk factors, what can you do to prevent glaucoma? Here are some guidelines for an eye healthy lifestyle that can prevent glaucoma, as well as many other eye and not-eye related diseases:

 - Don’t smoke
 - Exercise daily
 - Maintain a healthy weight
 - Prevent UV exposure (by wearing sunglasses, protective clothing and sunscreen when outdoors)
 - Get regular comprehensive eye exams - and make sure to tell your eye doctor if you have risk factors for glaucoma
 - Eat a healthy diet rich in a large variety of colorful fruits and vegetables, vitamins A, C, E and D, zinc and omega 3 fatty acids

Even if you have 20/20 vision, you may still have an asymptomatic eye disease such as glaucoma. Glaucoma Awareness is step one in prevention but there is a lot more to do to keep your eyes and vision safe. During January, make a commitment to take the following additional steps toward glaucoma prevention:

1. Assess your risk factors
2. Schedule a comprehensive eye exam and discuss glaucoma with your eye doctor. Even if you feel you have clear vision, it is worthwhile to book an eye exam in order to detect eye diseases such as this "Sneak Thief".
3. Adopt the healthy, preventative lifestyle guidelines outlined above
4. Spread the word. Talk about glaucoma to friends and family to ensure that they too can become aware and take steps to prevent glaucoma from stealing their sight.
