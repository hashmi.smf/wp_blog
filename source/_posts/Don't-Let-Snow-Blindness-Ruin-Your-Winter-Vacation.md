---
title: Don't Let Snow Blindness Ruin Your Winter Vacation
author: Westpointoptical
tags:
  - eye exam near me
  - optical in brampton
  - eye infection
  - westpoint optical eye wear
  - eye allergies
  - swollen eyes
  - eye allergy
  - snow blindness
  - what is snow blindness
  - sunburned eyes
  - snowblindness
  - sunburn on eyes
  - blindness symptoms

categories: Don't Let Snow Blindness Ruin Your Winter Vacation
date: 2021-03-30 18:40:00
---

# Snow Blindness 101: Symptoms, Causes

![](/images/snow-blindness-1.jpg)

Snowboarding is fun, just like any other kind of snow sports. Well, indeed it’s super duper fun as long as we don’t get injured.

However, snow blindness, i.e. sunburned eyes, is something we want to avoid during our trip.

In this article, we are going to talk about snow blindness and most importantly, how to avoid it.

## What is Snow Blindness?

![](/images/snow-blindness-2.jpg)

According to MedicineNet, snow blindness refers to a burn of the cornea by ultraviolet B rays. The medical term for snow blindness is radiation keratitis or photokeratitis, “photo” means light and “keratitis” means inflammation of the cornea.

Some of my snowboarder friends would use the term “Sunburned eyes” to describe it.

When you are surrounded by an environment filled with snow, it is very likely for you to get snow blindness if you don’t know how to protect yourself. Simply because the snowfield is going to reflect the sunlight.

Snow can reflect over 80% of the UV rays, when we go for snowboarding, i.e. the mountains, are usually located at higher altitudes and thus a stronger UV rays from the sun.

## The Causes of Snow Blindness

![](/images/snow-blindness-3.jpg)

If you are still confused about what snow blindness is, here’s a simple formula to help you better understand it:

**Sun’s UV + reflection from snow + strong UV at tall mountains – proper eyewear = Snow Blindness**

Snow blindness can occur even when **there is NO snow,** as long as there are highly UV rays reflective media such as water, white sands, or direct UV sources, like sunlamps and tanning booths. So make sure you protect your eyes well in all conditions.

Do keep in mind that **snow blindness can occur when there is NO sun too!**

One of my friends went snowboarding on a cloudy but bright day, he forgot his light color lens on that day and thought the lens of his goggle was too dark for that weather. So he snowboarded without wearing eyewear, no sunglasses, no goggles, he was totally fine during snowboarding.

Right after his ride, but then his eyes started feeling gritty and painful by around midnight. Such a situation existed until the next day so he went to visit a doctor and was told that he was suffered from snow blindness.

Therefore, please be very careful that you don’t even need to see the sun to be snow blind, it just needs to be bright enough and have enough UV source in the environment.

## Typical Symptoms of Snow Blindness

![](/images/snow-blindness-4.jpg)

Just like having sunburned eyes, symptoms of snow blindness include but not limited to pain, tearing, swelling, headache, gritty feeling, halos around lights, hazy vision, double vision, and temporary color change or loss of vision.

Different people may suffer from different symptoms. As a snowboarding instructor, I used to meet several friends and students suffering from snow blindness, some of them had very dry and painful eyes while others had a headache and watery eyes.

One of my friends suffered twice from snow blindness, the first time he caught snow blindness right after a long drive on a bright day without wearing a pair of sunglasses, his eyes were feeling gritty, dry and he was seeing halos by that time.

The second time he was snowboarding on a sunny day, wearing sunglasses with no UV protection for a very long period of time, that time he was having painful and dry eyes plus a headache. Both times he was told by the doctors he was suffering from snow blindness, so make sure you treat your eyes well as long as you are feeling uncomfortable!

**Tips:** Do be very careful because these symptoms may not appear until 6-12 hours after the UV exposure, which means by the time you realized you notice symptoms of snow blindness, you’ve already been in the sun too long.

**Remember:** the longer you are exposed to UV rays, the worse your eyes would be.

## Is Snow Blindness Permanent or Temporary?

In most cases, snow blindness is temporary, people suffering from snow blindness could recover after they have rested the eyes and received proper treatments.

However, rarely, loss of vision could be permanent if a patient’s eyes had been exposed to the reflected light for too long and lead to solar retinopathy.

## How Long Does It Take for Snow Blindness to Go Away?

![](/images/snow-blindness-5.jpg)

The length of recovery depends on how long your eyes were exposed to the UV sources, the longer you were exposed under the sun, the longer it takes for the symptoms to go away. Well, the strength of the UV rays does matters too.

However, most of the time it may take **24 to 48 hours** for the symptom to go away and it takes around **5 to 7 days** for a full recovery with good treatment. However, as mentioned, in very extreme cases, the damage could be permanent.

## Snow Blindness Treatment
Now that we know snow blindness is usually temporary, therefore most treatments are focusing on relieving the uncomfortable feelings. Below are some treatments you may consider 

  - Take off your contact lenses immediately and stop wearing them until you are fully recovered. Let your eyes rest for a few more days even those symptoms go away.
  - Stay indoors and wear sunglasses with UV protection, keep moisturizing your eyes with artificial tears to relieve the pain and discomfort. It is quite often to use Nonsteroidal anti-inflammatory drug (NSAID) eye drops to reduce inflammation and eye pain. Avoid using eye drops that are too thick which might lead to even blurred vision.
  - You may also take pain-killing pills to relieve the pain, but then just make sure you won’t be over dos and you are not allergic to that meds too.
  - Placing cool compresses or simply cool pads over your closed eyes could also help.
  - Make sure you don’t rub your eyes.
  - Go visit a doctor immediately if the symptoms persist more than 24 to 48 hours or they got worse after one day.

## How to Prevent Snow Blindness?

It is super duper easy!

All you need to do is: go find a pair of sunglasses or snow goggles that can block 99% or more of UV rays when you go snowboarding and skiing.

Those of you choose to ride in the mountain? Make sure you wear sunglasses with proper UV protection during driving. And for skiers and snowboarders, always wear good snow goggles.

Meanwhile, nowadays there is more and more welding helmet for snowboarding, just make sure the one you get can block 99% of UV rays too.

If you are getting a lighter color one, ensure you can fit a pair of sunglasses inside comfortably.

When you go skiing or snowboarding, always bring along a spare lens that’s with a different color tone, a combination of darker and lighter color options can ensure your eyes are well protected and comfortable under all weather conditions.

In case of an accident, for example, your goggles break and you need to wait for rescue, or you really need to move to a safe place by daytime.

Make sure you cover your eyes well with a dark but transparent fabric by bright time, or else, make a Slick Inuit Shade, a traditional snow goggles worn by the Alaskan and Canadian Arctic in the past.

## Final Words

So that’s all for what I can think of about snow blindness till now. Most of the time it is **nothing too serious or permanent.**

However, it could be very annoying since you cannot go skiing or snowboarding if you are suffering from snow blindness, and you can’t even watch TV or use your computer because your eyes need to rest.

Make sure you stay smart and wear proper eyewear to protect yourself.



