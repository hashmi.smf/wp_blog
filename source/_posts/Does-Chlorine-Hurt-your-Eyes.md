---
title: Does Chlorine Hurt your Eyes?
author: Westpointoptical
tags: 
 - eyeglass frames
 - optical near me
 - eye test game
 - eye test near me
 - eye test
 - free eye test
 - online eye test for glasses
 - coolwinks eye test
 - dry eye test
 - optician near me
 - best eyeglass frames brand
 - best eyeglass frames
 - trending eyeglass frames
 - sporty eyeglass frames
 - optical eyeglass frames
 - eye doctor near me
 - eye exam near me
 - eye test form
categories: Does Chlorine Hurt your Eyes?
date: 2021-04-05 19:24:20
---
![](/images/Does-Chlorine-Hurt-your-Eyes?.jpg)

Pools, lakes, water parks, and sprinklers are largely incredible for keeping you cool when the climate warms up. Be that as it may, is this additional time in the water hard on your eyes? Should you stress over chlorine and foreign substances causing eye aggravation 

This is what you need to think about keeping your eyes protected and sound in the various sorts of water they may experience

### Chlorinated water in pools
The purpose of utilizing chlorine is to keep pools and water parks as perfect and protected as could be expected. Generally, this item manages its work. But since it's synthetic, it can cause a response on the eye's surface. 

"Chlorine can make your eyes somewhat red, eyeglass outlines, sorrowful and delicate to light a few hours after you are in a pool or playing at a water park," says Dr. Gans. "Wearing swimming goggles diminishes openness to the substance. The most ideal approach to facilitate the agony is to flush your eyes with cool, clean water or a saline arrangement." 

Chlorine slaughters most hurtful things in the water, however not all that matters. Some infections (counting adenovirus and pink eye) and microbes may get by in chlorinated water and can cause contamination. 

Microorganisms and infections are bound to cause issues in the event that you as of now have a cut or disturbance in your eyes when you enter the water. Contact focal points, for example, can aggravate the eye's surface, making it bound to get tainted.

### Fresh water in lakes and ponds

In contrast to chlorinated water, new water in lakes and lakes can contain microbes and different living beings including acanthamoeba. This organic entity causes uncommon contamination that is hard to treat. It is likewise predominant in well water. 

This condition (acanthamoeba keratitis) happens all the more often in contact focal point wearers. The contamination enters the eye when it interacts with disturbance or a cut. You can likewise spread it when you contact your eyes with tainted water on your hands. 

Acanthamoeba keratitis is treatable with solution eye meds, yet early determination is significant. Whenever left untreated, it can cause visual hindrance or even visual deficiency. It is very uncommon, nonetheless, just happening in around 33 cases for each million contact focal point wearers.

### A quick note about water from a hose

One other water danger to look for becomes an integral factor when you utilize the nursery hose, a sprinkler, or water firearms to keep cool. 

"Water from a hose is commonly protected," says Dr. Gans. "Yet, remember (and remind your youngsters) that it's undependable to splash or shoot water at anyone at short proximity. Water hitting the eye at a high speed can cause harm."

### How to spot signs of infection
Typical signs of an eye infection include:

 - Redness. 
 - Pain. 
 - Discharge that is yellowish or bodily fluid-like. 
 - Vision issues. 
 - Sensitivity to light. 
 - Swelling.

Acanthamoeba keratitis presents with similar symptoms, but also can include excessive tearing and the sensation that you have something in your eye.

### Protect your vision with prompt treatment
If you suspect you may have an eye infection, it’s important to see an eye doctor immediately for an evaluation.

Treatment for eye infections depends on the cause. However, treatment may include:

 - Warm compresses to soothe the pain.
 - Eye drops.
 - Creams.
 - Antibiotics.

“If an infection isn’t treated correctly and quickly, it can cause damage and scarring to the retina,” he says. “And that can affect your vision long-term.” 

