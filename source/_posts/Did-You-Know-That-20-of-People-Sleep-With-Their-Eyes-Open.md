---
title: Did You Know That 20% of People Sleep With Their Eyes Open?
author: Westpont optical
tags:
  - westpoint optical brampton
  - optical in brampton
  - optical stores in brampton
  - types of eye tests
  - westpoint eye wear
  - brampton eye care
  - sunglasses brampton
  - eyehealth
  - eyecare 
  - vision
  - eye infection
  - eyesight
  - eye health tips
  - eye exams
  - eye test
  - eye exam cost brampton
  - optometrist near me
  - sleeping with open eye
  - nocturnal lagophthalmos
  - lagophtalmie

categories: Did You Know That 20% of People Sleep With Their Eyes Open?
Date: 2021-03-24 12:58:55
---

At any point heard the platitude "to lay down with one eye open"? It's by and large utilized as an analogy while encouraging one to remain cautious. In any case, laying down with eyes open is a typical eye and rest issue known as nighttime lagophthalmos. Truth be told, the National Sleep Foundation gauges that around 1 out of 5 individuals lay down with their eyes open.

This condition is tricky in light of the fact that it can meddle with rest and effect eye wellbeing. Individuals may not get as much rest, or rest as sufficiently as they'd like, because of the torment and inconvenience brought about by the eyes drying out during the evening.

![](/images/women-sleeping-with-openeye.jpg)

Nighttime lagophthalmos for the most part demonstrates a fundamental ailment, like a thyroid issue or an immune system issue. On the off chance that after waking you experience aggravated, dry, drained, red, or difficult eyes, or on the off chance that you presume you may be laying down with your eyes open, talk with our master Eye Examiner today.

## What Happens When You Sleep With Your Eyes Open?

Individuals who have nighttime lagophthalmos may not realize they have it. It is hard to assess whether your eyes are shut when you're in reality sleeping. In any case, some significant markers may highlight the condition, including: 

 - Eyes that vibe scratchy, disturbed, and dry 
 - Obscured vision 
 - Red eyes 
 - Eye torment
 - Tired eyes

For those with nighttime lagophthalmos, the eye loses the security of a shut top and gets got dried out, causing the tear layer to dissipate and the eyes to get dry. Nighttime lagophthalmos likewise lessens the eye's capacity to release foreign substances, for example, residue and flotsam and jetsam that fall into the eye during the evening. These pollutants can possibly prompt:

 - Eye infections
 - Corneal damage, such as corneal abrasion, sores and ulcers
 - Eye dryness and irritation
 - Poor quality sleep
 - Loss of vision

## Why Do We Close Our Eyes to Sleep?

There are a few reasons why it's imperative to close our eyes while we rest. Shut eyelids block light, which invigorates the cerebrum to alertness. 

Shutting our eyes additionally secures and greases up the eyes while we rest. On the off chance that your eyelids don't close, your eyes become more helpless to dryness, contaminations, and garbage that can scratch and harm the cornea.

## Why do Certain People Sleep With Their Eyes Open?

There are various reasons individuals may lay down with their eyes open. The most well-known explanations behind nighttime lagophthalmos include:

**Problems With Facial Nerves and Muscles**

Issues with facial nerves and muscles encompassing the eyelid can make the cover stay open during rest. The shortcoming in facial nerves can be described to a few elements.

 - Injury or trauma
 - Stroke
 - Tumor
 - Bell’s palsy, a condition that causes temporary paralysis or weakness of facial muscles.
 - Autoimmune disorders and infections, such as Lyme disease, chickenpox, Guillain-Barre syndrome, mumps, and several others.
 - Moebius syndrome, a rare condition that causes problems with cranial nerves.

**Damaged Eyelids**

Eyelids can get harmed because of medical procedures, injury, or sickness, making it hard to completely close the eyes during rest. Moreover, a condition known as floppy eyelid disorder can likewise meddle with eye conclusion and is regularly connected with obstructive rest apnea (OSA). OSA is regularly connected to eye sicknesses like glaucoma and optic neuropathy.

**Thyroid-Related Eye Problems**

A common symptom of Grave’s disease, a form of hypothyroidism, is protruding eyes. The bulging eyes, known as Graves’ ophthalmopathy, can prevent the eyes from closing.

**Genetics**

There additionally will in general be a hereditary segment to nighttime lagophthalmos, as it regularly runs in families. Whatever the reason, the side effects of nighttime lagophthalmos are awkward and the results can prompt visual difficulties.

## Can Nocturnal Lagophthalmos Be Treated ?

This condition can be treated in several ways, depending on the underlying cause and severity of symptoms. Treatments include:

 - Regulating **Artificial tears** for the duration of the day, giving a film of dampness around the eyes that secures them around evening time.
 - Wearing an **eye cover** or **goggles** to shield the eyes from outer flotsam and jetsam and visual incitement. These things are exceptionally intended to produce dampness for the eyes while you rest.
 - Utilizing a **humidifier**, which gives a dampness rich climate to keep your eyes from drying out.
 - Wearing **eyelid weights** to help keep the eyelids closed.
 - In acute cases, **surgery** may be recommended.

 Try to counsel your Brampton eye specialist prior to undertaking any of these medicines.

Since nighttime lagophthalmos at times flags a hidden condition, it is particularly essential to contact our Expert Eye Examiner in Brampton for an appropriate finding and to get quick treatment. On the off chance that nighttime lagophthalmos is left untreated for an all-encompassing period, patients hazard truly harming their eyes and vision.

At Westpoint Optical, we put your family's requirements first. Converse with us about how we can assist you with keeping a sound vision. Call us today: 905-488-1626 to discover our eye test arrangement accessibility. or then again to demand a meeting with one of our Brampton eye specialists.





