---
title: Are Your Eyes Sensitive to Light?
author: Westpoint Optical
tags:
 - eyeglass frames
 - optical near me
 - eye test game
 - eye test near me
 - eye test
 - free eye test
 - online eye test for glasses
 - coolwinks eye test
 - dry eye test
 - optician near me
 - best eyeglass frames brand
 - best eyeglass frames
 - trending eyeglass frames
 - sporty eyeglass frames
 - optical eyeglass frames
 - eye doctor near me
 - eye exam near me
 - eye test form
Categories: Are Your Eyes Sensitive to Light?
date: 2021-05-07 13:03:20
---
![Eyes Sensitive](/images/Are-Your-Eyes-Sensitive-to-Light?.jpg)
Light affectability, otherwise called photophobia, is a condition wherein splendid light - either regular daylight or counterfeit light - can cause huge distress, agony, and bigotry. Individuals that experience light affectability will end up expecting to close their eyes or squint when presented to light and regularly experience cerebral pains and sickness also. In gentle cases, the uneasiness goes with openness to splendid lights or brutal daylight, however, in serious cases, even a modest quantity of light can cause torment and inconvenience. 

Photophobia is more normal in people with light eyes. This is on the grounds that the more noteworthy measures of shade in hazier eyes help to shield the eye from the cruel beams of light. The hazier shade of the iris and choroid assimilates the light, as opposed to mirroring the light and causing interior reflection or glare experienced by those with lighter eyes. Individuals with albinism, which is a complete absence of eye color, additionally experience critical light affectability hence. 

Intense photophobia is normally an indication that goes with a condition like eye disease or disturbance (like conjunctivitis or dry eyes), an infection, or a headache (light affectability is quite possibly the most widely recognized side effects of headaches). It could likewise be brought about by something more genuine, for example, an eye condition like a corneal scraped area, a disconnected retina, uveitis or iritis, or a foundational infection like meningitis or encephalitis. Light affectability is likewise a result of refractive medical procedures (like LASIK) and a few prescriptions (like antibiotic medication and doxycycline).

### How to Deal with Photophobia
The best method to diminish the inconvenience brought about by photophobia is to avoid daylight and faint indoor lights however much as could be expected while you are encountering side effects. Wearing dull shades and keeping your eyes shut may likewise give some alleviation. 

In the late spring, it is more normal for UV to trigger corneal irritation (keratitis) and cause photosensitivity also. Wind and eye dryness can likewise set off photosensitivity, which are all the more valid justifications to wear shades.

If the sensitivity is new and the cause is unknown, you should seek medical attention immediately, especially if you experience any of the following symptoms:  

 - Blurry vision
 - Burning or pain in the eye
 - Fever and chills
 - Confusion and irritability
 - Severe headache
 - Drowsiness
 - Stiff neck
 - Nausea and vomiting
 - Numbness
 - Foreign body sensation

In situations where photophobia is an indication of a basic issue, treating the issue will probably cause help in your affectability. This will change contingent upon the affliction however could incorporate agony drugs, eye drops or anti-infection agents, or mitigating prescriptions. On the off chance that the affectability is gentle because of your hereditary inclination or an aftereffect of medical procedure, ensure you take as much time as is needed you take off from the house. Individuals who wear remedy eyeglasses may consider photochromic focal points which consequently obscure when presented to light.

### What Causes Photophobia?
Light-sifting safeguards are useful on the off chance that you are delicate to daylight or even solid indoor lighting. Shown are Cocoons Sidekick flip-up safeguards that are intended to work with remedy eyeglasses. They are accessible in five distinct tones, each with its own light transmission and glare decrease ability. Your eye care proficient can suggest the best one for your requirements. 

Light affectability additionally can be an indication of basic sicknesses that don't straightforwardly influence the eyes, for example, infection-caused diseases or extreme cerebral pains, or headaches. 

Individuals with a lighter eye tone likewise may encounter all the more light affectability in conditions like splendid daylight, on the grounds that hazier-hued eyes contain greater shade to secure against unforgiving lighting.