---
title: Can you Really Go Blind from Looking at a Solar Eclipse?
author: Westpoint Optical
tags:
 - vision problems
 - types of vision problems
 - eye vision problems
 - vision problems symptoms
 - double vision problems
 - peripheral vision problems
 - common vision problems
 - long distance vision problems
 - computer vision problems
 - causes of vision problems
 - vision ias current affairs
 - vision
 - computer vision
 - vision meaning
 - double vision
 - vision synonyms
 - blurred vision
 - vision and mission
 - vision express
Categories: Can you Really Go Blind from Looking at a Solar Eclipse?
date: 2021-04-15 12:09:18
---

![Looking at a solar eclipse](/images/Can-you-Really-Go-Blind-from-Looking-at-a-Solar-Eclipse?.jpg)

## What is a solar eclipse?
A sun-oriented shroud happens when the moon moves between the sun and the earth. The moon makes the light of the sun be impeded from arriving at the earth, projecting a shadow on the earth. An absolute sun-based overshadowing is a point at which the moon totally impedes the sun. The sun's external environment (called the sun-based crown) gleams around the moon when it is obstructing the sun. An incomplete sun-powered overshadowing is a point at which the moon just squares part of the sun. Survey a halfway sun-powered overshadowing can open your eye to the sun's beams making harm the eye.

## How can your eyes be affected by a solar eclipse?
Presenting your eyes to the sun without legitimate eye insurance during a sun-oriented obscuration can cause "overshadow visual deficiency" or retinal consumption, otherwise called sunlight-based retinopathy. This openness to the light can cause harm or even annihilate cells in the retina (the rear of the eye) that send what you see to the cerebrum. This harm can be transitory or lasting and happens with no torment. It can require a couple of hours to a couple of days in the wake of review the sun-powered obscuration to understand the harm that has happened.

## What are the eye symptoms that can occur from looking at a solar eclipse without proper eye protection?

 - Loss of central vision (solar retinopathy)
 - Distorted vision
 - Altered color vision

 If you notice symptoms after viewing a solar eclipse, seek treatment from an eye care professional.

## How to safely watch a solar eclipse
The lone time that you can securely see a sun-based shroud without extraordinary gear is during absolute sunlight-based obscuration. This is the point at which the moon totally covers the sun. It is never protected to take a gander at an incomplete sunlight-based shroud without appropriate security gear or strategies. During the exceptionally short time, the sun is altogether sun-powered overshadowing it is protected to see it, yet do as such with alert. In any event, during the complete sun powered shroud, the all-out obscuration may last just a brief timeframe, and on the off chance that you are looking towards the sun as the moon moves from impeding the sun, you may get a sun based consumption on your retina which can make perpetual harm your eyes. Talk with your eye care proficient to decide the best survey alternative for you. The following are a couple of normal approaches to securely watch a sun oriented obscuration

## How not to watch a solar eclipse
Be careful about how you watch a solar eclipse. It is not recommended to view it in the following ways:

**Smartphone:** Watching a solar eclipse on your smartphone camera can put you at risk of accidentally looking at the sun when trying to line up your camera. It could possibly also damage your smartphone camera. Don’t take the risk.

**Camera viewfinder:** Never look at a solar eclipse through the optical viewfinder of a camera. It can damage your eyes in the same way as looking directly at it.

**Unsafe filters:**  Unless specifically designed for viewing a solar eclipse, no filter is safe to use with any optical device (telescopes, binoculars, etc). All color film, black-and-white film that contains no silver, photographic negatives with images on them (x-rays and snapshots), smoked glass, sunglasses (single or multiple pairs), photographic neutral density filters and polarizing filters are unsafe filters to watch a solar eclipse. Also, solar filters designed for eyepieces that come with inexpensive telescopes are also unsafe. All of these items can increase your risk of damaging your eyes.

## Solutions for Viewing an Eclipse
There are a few options for safely viewing these rare events. First of all, you can purchase special eclipse glasses which are glasses made with specific lenses that block out dangerous wavelengths of light. Alternatively, you can make a pinhole projector which will project a miniature image of the eclipse onto the ground through a piece of cardboard or paper with a hole in it.


