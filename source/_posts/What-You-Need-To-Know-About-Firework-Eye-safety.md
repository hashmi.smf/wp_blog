---
title: What You Need To Know About Firework Eye safety
author: Westpoint Optical
tags:
  - eye safety
  - eye protection
  - eye safe
  - firework safety
  - consumers fireworks
  - lighting fireworks
  - firework safety tips
  - westpoint optical eyewear
  - optical near me
  - sunglasses
  - eye sight
  - eye exam cost brampton
  - optometrist near me
  - eye test
  - eye health tips
  - firecrackers safety
  - eye doctor

categories: What You Need To Know About Firework Eye safety
date: 2021-03-24 17:06:47
---

Independence Day may have passed however firecrackers season is still going full bore and firecrackers related injury and demise is a genuine and genuine risk. As indicated by the 2014 Annual Fireworks Report, assembled by the Canadian Consumer Product Safety Commission, there were at any rate 11 passings and 10,500 wounds because of firecrackers last

While the most widely recognized wounds happened to the hands and fingers (around 36%), around 1 in each 5 of the wounds (19%) were to the eyes, where injuries, cuts and unfamiliar bodies in the eyes were the most well-known wounds. The peril to the eyes is not kidding and can bring about lasting eye harm and loss of vision. Firecrackers can crack the globe of the eye or cause compound and warm consumes, corneal scraped spots and retinal separation.

![](/images/firework-eyewear.jpg)


Unfortunately, youngsters from 5-9 years old had the most elevated assessed pace of crisis division treated firecrackers related wounds (5.2 wounds per 100,000 individuals) and kids under 15 years of age represented 35% of the absolute wounds. Almost 50% of those harmed were observers and not really taking care of the actual firecrackers.

**Here are Five Fireworks Safety Tips to enjoy fireworks safely:**

  - The most secure approach to see firecrackers is at an expert public presentation as opposed to at-home utilize.
  - When seeing firecrackers, cautiously cling to the security obstructions and view them from in any event 500 feet away.
  - Never contact unexploded firecrackers. Contact nearby fire or police divisions promptly to manage them.
  - Never let little youngsters play with firecrackers even sparklers.
  - In situations where customer firecrackers are lawful, use them securely. Anybody that handles firecrackers or is a spectator should wear legitimate defensive stuff and eyewear that fulfill public wellbeing guidelines.
  - Proficient evaluation firecrackers should just be dealt with via prepared pyrotechnicians.

On the off chance that a firecracker-related eye injury happens, look for clinical consideration right away. Attempt to let the eye be however much as could reasonably be expected; don't rub or flush the eyes, apply pressing factor or attempt to eliminate an item that has entered the eye.

As well as knowing the threats and security insurances yourself, it's imperative to show your youngster's firecracker wellbeing. Continuously recall that while they are amusing to appreciate in the correct setting, firecrackers are hazardous gadgets and ought to be treated thusly.

At Westpoint Optical, we put your family's requirements first. Converse with us about how we can assist you with keeping a sound vision. Call us today: 905-488-1626 to discover our eye test arrangement accessibility. or then again to demand a meeting with one of our Brampton eye specialists.



