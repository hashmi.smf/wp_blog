---
title: Is Too Much Screen Time Dangerous For Your Kids?
date: 2021-03-30 17:32:17
tags:
  - kids eyes	
  - parts of eyes
  - child eyes
  - kids eye exam
  - pain behind eye
  - why do my eyes hurt
  - westpoint optical stores
  - optical in brampton
  - eye test near me
  - westpoint eyewear
  - eye exam near me
  - contact lenses
---
## How much screen time is too much? Follow these guidelines for your child to avoid harmful health effects

![](/images/kids-with-laptop.jpg)

In the Canada, kids ages 8 to 12 spend an average of 4 to 6 hours per day on screens.

  - Children younger than 5 years old should have very limited screen time and mainly use screens for educational purposes. 
  - For children older than 5 and adults, there are not as strict guidelines, but you should be careful that screen time doesn't interfere with relationships or responsibilities. 
  - Too much screen time has been shown to increase the risk for behavior problems in children, obesity, trouble sleeping, and back and neck issues.

Both kids and adults are spending more and more time using screens – for everything from work and school to socializing and having fun. But spending too much time using screens can have harmful consequences for your health, your emotions, and brain development in children. 

Here's what you need to know about how much screen time is healthy and when you need to set limits.

## How much screen time is healthy

In the Canada, kids between ages 8 and 12 spend an average of 4 to 6 hours per day looking at screens, while teenagers may spend as much as 9 hours per day. Adults in the US spend even more time on screens – on average just over 10.5 hours each day.

There are no hard and fast guidelines for how much screen time you should get as an adult. But for kids, experts have recommendations based on age, to ensure that screen time won't interfere with a child's development.

**0 to 18** months: Babies younger than 18 months shouldn't be getting any screen time unless they are video chatting with family members. "Instead, the recommendations are to focus on and encourage play, reading, and interactions between parent and child," 

**18 to 24 months:** At this age, kids can have some screen time, but it should be limited to watching educational content with a parent or caregiver. "We recommend they focus on exposing them to high-quality programming and apps such as PBS Kids, Sesame Workshop, and things discussed in Common Sense Media," Mattke says.

**2 to 5 years:** At this point, kids can start to have recreational screen time outside of education, but it should be limited. The Canadian Academy of Child & Adolescent Psychiatry recommends getting no more than one hour on weekdays and three hours on weekend days.

**Older than 5:** There is no one-size-fits-all approach for how much screen time older kids and adults should get, Mattke says. A general rule of thumb is that screen time shouldn't interfere with learning, relationships with peers and family, physical activity, sleep, or their mental health. 

For older kids and adults, it's hard to put an exact number on how much screen time is too much. However, research shows that there are many ways excessive screen time can damage your physical and mental health.  

## Negative effects of too much screen time
Here are some of the most common issues that can arise when you spend too much time using screens. 

### Behavior and learning problems in children

Putting on the TV or playing a YouTube video may calm your child in the moment, but too much screen time can lead to behavioral issues over time. "Excessive TV viewing is associated with delays in cognition, language, and social-emotional development," Mattke says.

A 2015 study found that two-year-olds who watched more than three hours of TV per day were three times more likely to have a delay in their language development, compared with toddlers who watched less than one hour. This may be because children learn more easily from interacting with people and objects compared with a screen.

Too much screen time can also harm children's' and teens' mental health. A very large 2018 study found that teens who used screens for 7 or more hours per day were twice as likely to be diagnosed with depression or anxiety, compared with those who used screens for less than one hour.

The type of media a child sees can also have an effect on their behavior, as children can imitate actions they see on TV as early as 6 months old. "There are strong associations between violent media content and child aggressive behavior," Mattke says.

**Obesity**
Most of the time when you are looking at a screen, you are sitting or lying down. This means that as you spend more time on screens, you also spend more time being sedentary, which can increase your risk of obesity, along with chronic issues like heart disease.

Being in front of a screen can also make you more likely to mindlessly snack. A 2008 study found that children who cut down their screen time by 50% ate significantly fewer calories than those who kept their normal screen time schedule.

Adults who spend more time on screens are also at greater risk for obesity. A very large 2003 study that followed middle-aged women over six years found that for every two hours spent watching TV each day, women were 23% more likely to become 
obese

**Sleep problems**
"There is a growing body of evidence that suggests that media use negatively affects sleep for both children and adults," Mattke says. This is partly because electronic devices emit a type of blue light that can lower your levels of melatonin, a hormone that regulates when you sleep and wake up.

A 2019 study found that too much screen time can lead to symptoms of insomnia in teenagers. Teens who used screens for more than 3 hours per day had a significantly harder time falling asleep than those who used screens less often.

The negative effect of screens may be most harmful in the hours before bedtime. About 90% of Americans say they use a technological device less than an hour before they go to bed, which can reduce the essential rapid eye movement (REM) sleep that makes you feel rested the next morning.

Experts recommend that you stop using screens ideally two hours, or at the least 30 minutes before you go to sleep.

**Back and neck issues**

Looking down at a phone or tablet screen for too many hours can put a lot of strain on your neck muscles and the small bones at the top of your spine. This is because it creates a slumped posture which is unnatural for us to keep for extended periods.

A very large 2010 study found that teens who spent more time using screens were more likely to report having headaches and backaches than less frequent screen users.

If you need to spend extended time using a screen, experts advise that you use a device to prop up the screen so you can see it without bending your head forward.

### The bottom line

It's almost impossible to avoid using screens in our daily lives, but we can set limits on how many hours we spend on devices. For kids, it's best to follow expert guidelines to avoid early issues with learning and behavior. As an adult, you need to decide what works best for you – if you are concerned that your screen use is causing issues, talk to your doctor or a mental health provider about how you can adjust your daily habits.
