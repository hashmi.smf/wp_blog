---
title: Eye Health Benefits of Eating Pumpkin
author: Westpoint Optical
tags: 
  - benefits of pumpkin seeds
  - pumpkin seeds
  - health benefits of pumpkin seeds
  - pumpkins benefits
  - how to eat pumpkin seeds
  - pumpkin nutrition
Categories: Eye Health Benefits of Eating Pumpkin
date: 2021-04-03 13:20:54
---

![Benefits of Pumpkin for your Eyes](/images/pumpkin.jpg)

Pumpkins are not only good for carving and baking pies, but they're also good for your eyes.

Pumpkin, a carotenoid, is rich in nutrients linked to eye and overall health, including **age-related macular degeneration** (AMD) and **cataracts**. Five years ago, when the Centers for Disease Control and Prevention (CDC) ranked **"powerhouse fruits and vegetables"** based on nutrient density scores, pumpkin came in No. 20 out of 41-below broccoli but ahead of Brussels sprouts. (Watercress ranked No. 1.)

Consuming these superfoods is "strongly associated" with reducing risks for chronic diseases. This fall, with pumpkins plentiful in decoration and dessert, provides an opening for doctors of optometry to talk to patients about nutrition and eye health.

"Pumpkin, that fall favorite fruit, contains vitamins A, C and E, zinc, fiber, lutein and zeaxanthin-which are all beneficial to the eyes," says Georgia Air National Guard Lt. Col. Jennifer Carver, O.D. Dr. Carver, who also has a culinary and baking/pastry degree, often speaks to her patients about basic nutrition.

Adds Matthew Houck, O.D., who practices in Iowa City, Iowa: "I really do stress the importance of nutrition through natural food sources with my patients, especially those diagnosed with certain eye conditions. I think it is an important topic to bring up with patients, as vitamin supplements have exploded into our markets. Vitamin supplements are great, but they do not replace good nutrition."

According to the CDC, 4 in 10 children and fewer than 1 in 7 adults eat the daily recommended amounts of fruit.

## Benefits of healthy food for vision

The groundbreaking Age-Related Eye Disease studies (AREDS), sponsored by the National Eye Institute, solidified the link between eye health and nutrition. The study showed that individuals at high risk for AMD could slow the progression of advanced AMD by about 25% and visual acuity loss by 19% by getting 40-80 mg/day of zinc, along with certain antioxidants.

Other studies have also shown that nutrients, either through food or vitamin supplements, can help preserve vision. They include:

  - Lutein and zeaxanthin, found in green, leafy vegetables, reduce the risk of AMD and cataracts.
  - Vitamin E, an antioxidant found in nuts, cereals and sweet potatoes, protects cells in the eye from damage from free radicals.
  - Vitamin C, found in fruits and vegetables, lowers the risk for cataracts and slows the progression of AMD.
  - Zinc, an essential trace mineral in red meat, seafood and chicken, is concentrated in the retina and choroid. Poor night vision and cataracts have been linked to zinc deficiency in the body.

Nutrition is important in eye health, but there's no substitute for a regular, comprehensive eye examination with a doctor of optometry to help patients preserve their vision.

## Picky about pumpkins

Not all pumpkins are created equal-some are better than others for cooking, says Dr. Houck, who appeared on the popular Fox television show, "Master Chef," for several weeks in 2018.

"Decorative pumpkins are slightly different than the more edible versions," he says. "Most grocery stores call the edible versions 'pie pumpkins,' which are much smaller than the ones we carve up with scary faces."

The fresher (canned pumpkin contains more sugar) the better, he adds.

"My favorite way to prepare pumpkin and winter squash is to roast it in the oven with some oil, salt and pepper until the edges begin to brown and the sweetness intensifies," Dr. Houck says. "Roasting really brings out the natural sweetness and makes pumpkin even more delicious."

## Pumpkin Soup - Food for the Soul and Your Eyes

  - 2 sugar pumpkins or 2 ¼ cups of pureed pumpkin 
  - 1 Tbsp olive oil 
  - 2 shallots, diced 
  - 3 garlic cloves, minced 
  - 1 cup coconut milk or other non-dairy milk 
  - 2 cups vegetable stock 
  - 2 Tbsp nectar or maple syrup 
  - ¼ tsp salt 
  - ¼ tsp every one of nutmeg, dark pepper, cinnamon

Start by preheating your stove to 350 degrees and line a heating sheet with material paper. 

At that point, cut the tops off the pumpkins and cut them down the middle. Scratch out the entirety of the seeds with a sharp spoon. Here's a tip: keep the seeds as an afterthought and dish them later for nibbling. 

Brush the substance of the pumpkins with olive oil and spot them on the material paper, cut side down. Spot in the stove for 40-50 minutes, until a fork effectively punctures the skin. 

Eliminate the pumpkins from the broiler and let them adequately cool to deal with. Eliminate the skin from the pumpkin and put it in a safe spot.

In a medium pot put over medium/high warmth, add the olive oil, diced shallots, and garlic. Cook until clear or marginally seared, mixing at times. 

Add the pumpkin and remaining fixings to the pot and stew for 20 minutes. 

Utilize a drenching blender to puree the soup into a thick and rich bisque. 

Serve hot and appreciate!

To learn more about eye health, schedule an eye exam at [Westpoint Optical Eye Care](https://westpointoptical.ca/) today.

