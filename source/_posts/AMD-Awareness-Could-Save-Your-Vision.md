---
title: AMD Awareness Could Save Your Vision
author: Westpoint Optical
tags:
  - sun with sunglasses
  - sun glasses
  - eye care
  - westpoint optical stores
  - optical in brampton
  - eye test near me
  - westpoint eyewear
  - eye exam near me
  - contact lenses
  - kids eye wear
  - eye doctor
  - optical near me
  - contact lenses for kids
  - contact lens
  - contact lens case
  - eye sight
  - eye health 
  - westpoint optical eye wear
  - contact focal point
  - eye safety
  - eye doctor
Categories: AMD Awareness Could Save Your Vision
date: 2021-05-08 16:01:50
---
![Save your Vision](/images/AMD-Awareness-Could-Save-Your-Vision.jpg)
It's that season once more. Every February, the optometric local area groups together to make mindfulness about age-related macular degeneration (AMD). AMD is the main source of vision misfortune for individuals 50 years and more seasoned; early recognition assumes a critical part in the result of the infection. That is the reason carrying attention to the sickness and its danger factors are so significant. 

Macular degeneration is an infection that harms the macula, which is a little region in the focal point of the retina liable for sharp, clear focal vision. The infection comes in two structures, wet AMD and dry AMD. The most well-known structure, dry AMD, which influences around 80% of AMD patients, is the point at which the macula continuously diminishes, and little groups of a protein called drusen start to develop. Drusen result from cells in the macula that can't free themselves of metabolic waste called lipofuscin. The lipofuscin collects as drusen which causes a slow vision decay. 

Dry AMD can transform into wet AMD when strange fresh blood vessels develop through breaks in a film layer of the diminishing macula. The delicate veins release liquid into the macula, causing a fast lessening in focal vision. The wet structure is more uncommon, yet it can cause a quicker and more intense vision misfortune. In the event that an individual has dry AMD which transforms into wet AMD, this ought to be treated at the earliest opportunity, as inside the space of days this can cause lasting scarring. Luckily, there is a successful treatment for wet AMD whenever identified prior to scarring emerges. 

The two types of AMD bring about a deficiency of focal vision, while fringe vision stays unblemished. Indications can present as trouble zeroing in on objects before you, or an obscured or dulled territory in the focal visual field which prompts experiencing difficulty perusing, accomplishing close work, driving, or in any event, perceiving faces. With time, the size of the obscured region can develop and in the long run form into dark spots in focal vision. Intermittently patients don't see indications until a lot of harm has been finished. This is the reason standard eye tests are basic, particularly in the event that you are in danger. 

While AMD alone will not reason total visual impairment, it can cause a lasting, all-out loss of focal vision if not treated. Vision misfortune can prompt a condition called low vision which can genuinely affect everyday living and require a ton of help both by vision gadgets and the assistance of others.

### Are You at Risk? 
As it is an age-related infection, age is a critical danger factor for AMD, explicitly once you arrive at 60. Be that as it may, age isn't the solitary danger factor. While some dangerous factors for AMD can't be controlled there are a way of life factors that you can change to forestall AMD.

### Other than age, risk factors include:
 - **Family ancestry:** If you have a family background of AMD, you are more in danger. Exploration has recognized at any rate 20 qualities that are related to AMD, appearing there is a hereditary factor. 

 - **Race:** Caucasian plunge is a higher danger factor for AMD, and indeed, Caucasians with light irises have an expanded danger from age 50. 

 - **Smoking:** Smoking pairs your danger of creating AMD. 

 - **Overweight/Obesity:** Research shows that being overweight is a dangerous factor for AMD. 

 - Having a coronary illness, hypertension and elevated cholesterol increase your dangers. 

 - **Diet:** An unfortunate eating routine wealthy in soaked fats is a huge danger factor. 

 - Early openness to UV light and blue dangerous light (particularly with the more youthful age having expanded openness to computerized gadgets) can cause beginning stage AMD.

### Here are some lifestyle steps you can take to reduce your risk of AMD:
 - Stop smoking
 - Eat a healthy diet rich in omega 3 fatty acids, (from fatty fish or flax seeds), leafy greens and colorful fruits and vegetables. 
 - Know your family history.
 - Exercise regularly.
 - Protect your eyes using UV protection and blue blocker coatings on eyeglasses.
 - Get regular eye exams.

Notwithstanding a complete eye test, your PCP might have the option to test for certain danger factors. For instance, there is presently innovation accessible that can test for lutein levels through innovation like QuantifEye and Macuscope, (low lutein can show expanded danger). What's more, hereditary testing is additionally now accessible through a basic cheek swab to decide a person's danger for creating AMD.

### Treatment
There is no known solution for AMD, in any case, there are medicines accessible that may moderate the movement of the sickness. For dry AMD, considers (AREDS and AREDS2) have reasoned that a specific high-portion mix of dietary enhancements taken day by day can moderate the infection. The combo incorporates nutrients C and E, Lutein, Zeaxanthin, Zinc, and Copper. For wet AMD, the objective is to lessen the development of strange veins and the spillage that happens and this is done through specific drugs called against VEGFs which are infused into the eye or with laser medical procedure.

