---
title: Eye Am Home for the Holidays – 7 Eye Tips for College Students
author: Westpoint Optical
tags:
 - eye care
 - eye care near me
 - eye care hospital
 - eye care hospital near me
 - eye care tips
 - vision eye care
 - eye care tips for computure users
 - child eye care tips
 - for eye care tips
 - best eye care tips
 - daily eye care tips
 - eye care for kids 
 - eye care for students
 - summer eye care tips
Categories: Eye Am Home for the Holidays – 7 Eye Tips for College Students
date: 2021-04-13 18:41:15
---

![Student](/images/Eye-Am-Home-for-the-Holidays–7-Eye-Tips-for-College-Students.jpg)

Winter break is in half a month and, with undergrads discovering their route home for these special seasons, it is a fun time for guardians to check in and ensures their free children are dealing with themselves appropriately. Vision assumes a vital part in learning just as extra-curricular exercises and undergrads, specifically, are defenseless to a large group of eye and vision issues including wounds, contaminations, and expanded myopia. Here are 7 hints for undergrads to keep their eyes and vision protected and solid during the semester.

## 1. Wash your hands frequently.
School dormitories and swarmed homerooms can be a favorable place for germs and microorganisms, perhaps the most well-known of which is conjunctivitis or pink eye. To ward the germs off and stay solid, wash your hands consistently with cleanser and warm water and do whatever it takes not to contact your eyes

## 2.Take care of your contact lenses.
With the late evenings and occupied school life, it very well maybe not difficult to get remiss with contact focal point care, however, don't! Continuously cling to your eye specialist's guidelines for appropriate contact focal point cleanliness. Try not to rest in your contacts on the off chance that they're not endorsed for broadened wear, clean and store appropriately, just use contact focal point arrangement, and don't swim or shower with your focal points in. As well as causing dry eyes and aggravation, ill-advised consideration of focal points can bring about genuine contaminations and in the most pessimistic scenarios perpetual scarring and vision misfortune.

## 3.Take a break.
Numerous long periods of consideration can negatively affect you and in the present computerized age, the outcomes could be much more emotional. Blue light from PCs, tablets, and cell phones has been connected to vision entanglements and PC vision conditions which can cause obscured vision, migraines, and neck and shoulder torment. In the event that you are working at a PC or before a screen for quite a long time at a time, follow the 20-20-20 standard – like clockwork enjoy a reprieve and take a gander at something 20 feet away for 20 seconds. On the off chance that you go through the greater part of your day on the PC consider buying a couple of PC glasses to decrease the impacts of the screen on your eyes.

## 4.Get out.
Help yourself out and get outside consistently. Studies show that in excess of 50% of school graduates are partially blind, with eyesight deteriorating with each school year. Further examination has shown that investing more energy outside can shield vision from deteriorating.

## 5. Handle Makeup with Care.
Cosmetics, especially fluid or rich eye cosmetics, can be a favorable place for irresistible microorganisms. Never share cosmetics with companions and on the off chance that you move an eye disease discard your cosmetics as quickly as possible. A decent general guideline is to supplant all eye cosmetics like clockwork.

## 6.Use Eye Protection.
In the event that sports are essential for your school insight, ensure you are protecting your eyes with appropriate eye and vision gear. Defensive, polycarbonate, sports glasses, skiing, and swim goggles can shield your eyes from scratches, knocks, wounds, or more awful.

## 7.Get a Yearly Eye Exam.
As referenced above, it is basic for understudies to encounter a decrease in vision which could have an effect all through class. Get a yearly test to ensure you can see your best and that your eyes overall are solid. On the off chance that you appreciate sitting at the rear of the auditorium, your eye exam can guarantee you have refreshed glasses or contact focal points at your ideal vision.
