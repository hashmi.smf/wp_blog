---
title: Understanding The Eye Chart
author: Westpoint Optical
tags: 
  - eye chart
  - snellen chart
  - vision test
  - eye test
  - 20/20
  - eye exam
  - astigmatism test

Categories: Understanding The Eye Chart
date: 2021-04-06 16:14:40
---

![The Eye Chart](/images/eye-chart.jpg)

## The Eye Chart

The eye chart just might be the most recognized symbol of an eye doctor’s office. Almost everyone has had to read letters from one at some point in their life whether at the eye doctor’s office, your pediatrician or school nurse’s office, or even the digital eye chart at the DMV. Find out why this chart has remained part of our medical technology for over 100 years and how it can help your vision.

## What Is 20/20 Vision?

Before understanding the eye chart, it is important to know what 20/20 vision actually means. It is a numerical reference depicting how well you can see objects from a distance of 20 feet compared to someone with a “perfect” vision looking at the same object at 20 feet. This fraction of how well you see is called visual acuity. If you have poor vision and lost your glasses, your visual acuity may be 20/100. This means that the smallest line on the eye chart that you can read at 20 feet can be read by someone with **perfect vision** who is standing 100 feet away.

## How Do Eye Charts Work?

There is quite a bit of science going back over 100 years behind the eye chart. The most common eye charts used in America today are called Snellen charts, named after Hermann Snellen, a Dutch ophthalmologist in the 1800s. They start with a single, large letter E at the top of the chart. Each line below gets incrementally smaller until the letters are so small that the only people who have better than 20/20 vision can read them. The size of the letters is precisely calculated to represent perfect adherence to the visual standard of the 20/20 scale.

Even the types of letters used are carefully chosen. The next time you are looking at an eye chart, you will notice that it does not contain every letter of the alphabet. The only letters used are C, D, E, F, L, N, O, P, T and Z. The reason for this is that some letters can be identified by the human brain even if they are too blurry to be seen clearly. An example of this would be the letter Q. The brain is able to pick up on the shadow of the tail that comes off that letter and determine what it is, because there are no other letters like it.

## How Can People See Better Than 20/20?

Many are born naturally with 20/15 vision or better. Patients who have had their vision corrected with LASIK surgery are often able to see better than 20/20 for many years after their procedure. Contact lenses can sometimes be prescribed for patients to give them better than 20/20 vision as well.

The eye chart is one of the best ways to determine whether or not you need glasses or **contact lenses.** Next time you visit your optometrist, you can see how the lines you read on the eye chart reveal your visual acuity!

**Eye Didn’t Know That!** Many baseball teams employ eye doctors to fit their players with contact lenses to enhance their vision beyond normal limits and give them an advantage on the field. It is said that Ted Williams had a vision that was close to 20/10, which is about the limit of the human eye for visual acuity. This served to help him in his career as a decorated fighter pilot. He also claimed he was able to see the laces on the baseball as the pitcher threw it to know what kind of pitch it was!

