---
title: What You Should Know About Night Blindness
author: Westpoint Optical
tags: 
  - night blindness
  - night blindness symptoms
  - night blindness treatment
  - night vision problems
  - lasik night vision
  - congenital stationary night blindness
  - dry eye at night
  - csnb
  - westpoint optical eyewear
  - optical near me
  - sunglasses
  - eye sight
  - eye exam cost brampton
  - optometrist near me
  - eye test
  - eye health tips
Categories: What You Should Know About Night Blindness
date: 2021-03-26 15:19:34
---

## Night Blindness: Causes, Symptoms, and Treatment

![Night Blindness](/images/Night-Blindness.jpg)

Perhaps you have found yourself struggling to see as well as you used to when driving at night or even just around your house early in the dark or dim light hours. Maybe you’ve noticed that your eyes do not adjust quite as they used to when you would go from a brightly lit place (like outside) to a place of poor lighting (like a restaurant).

You may have heard a term thrown around called “night blindness”, but that’s not entirely correct. While night blindness refers to not being able to see at all in the dark or dimly lit places, the correct medical term is nyctalopia. People who suffer from this type of vision impairment, experience difficulty seeing or driving in darkness.

There are different types of night blindness, and depending on what causes night blindness for each person, dictates whether it is treatable or not. Night blindness treatment will be discussed below, as well what is night blindness exactly, and its causes.

## Night Blindness Symptoms and Treatments

The main night blindness symptom is, obviously, difficulty seeing in the dark, but different degrees of severity can be seen when transitioning from places of different brightness or while driving, due to the intermittent headlights, streetlights or road signs.

All night blindness symptoms are related to poor lighting situations. There are several causes that can be attributed to night blindness including:

  - **Nearsightedness (Myopia)**

  This is a refractive error where there is too much curvature in a person’s cornea or lens to allow light to focus properly on the retina. Objects that are closer in distance, appear clearer than ones further away. Nearsightedness can be fixed with contact lenses, glasses, or refractive surgery.

  - **Cataracts**

  This medical condition results in the lens of the eye progressively becoming more opaque, typically with age, which results in **blurred or cloudy vision.**

  - **Retinitis Pigmentosa**

  This condition is often labeled “tunnel vision” because dark pigment collects in the retina, which gives the vision of being in a tunnel.

  - **Usher syndrome**

  This condition affects both hearing and vision, and the most common symptom of the vision is retinitis pigmentosa.

  - **Glaucoma and Glaucoma medicine**

  Glaucoma is a disease of the optic nerve that can lead to poor vision and even blindness. Glaucoma medicine has a side effect that narrows the pupils, which limits the amount of light that can enter the eye, especially in low light situations.

![Symptoms Of Night Blindness](/images/symptoms-of-night-blindness.jpg)

  - **Low Vitamin A**

  While rare to occur in the Canada, nutritional deficiencies can lead to a variety of vision and health problems. Vitamin A is crucial for the proper function of the rods (specialized cells in our eyes that help us see light).

There are a variety of treatment options available and depending on what the underlying cause is, the options can include:

 - New glasses prescription to account for the degree of nearsightedness
 - Increased intake of vitamin A supplementation, to help improve the functioning of the rods, and to help counteract retinitis pigmentosa.
 - Surgery for cataracts
 - Changing glaucoma medicines

Whatever the case may be for the night blindness, difficulty seeing in dim environments can be frustrating and even dangerous, especially for the older population. If you struggle to navigate around your home in the dark or find yourself struggling to see clearly when driving at night, schedule an appointment with your eye doctor. Even if the cause is something harmless and curable, any sudden changes in vision are worth getting checked out.

If your daytime vision has begun to struggle or you are tired of dealing with the hassle of glasses and/or contacts, then give us a call, today. At Westpoint Optical, we are proud to offer the most cutting-edge technologies and treatments to help our patients see as clearly as possible.

To learn more about the procedures we offer,[visit our website.](https://westpointoptical.ca/contact.html)




