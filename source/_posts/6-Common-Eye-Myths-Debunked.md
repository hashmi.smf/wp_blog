---
title: 6 Common Eye Myths Debunked
author: Westpointoptical
tags:
 - eye health
 - eye health tips
 - eye health vitamins
 - food for eye health
 - foods for eye health
 - eye health clinic
 - eye health food
 - eye health foods
 - vitamins for eye health
 - eye health facts
 - best foods for eye health
 - eye health centre
 - fruits for eye health
 - eye health supplements
 - how to improve eye health
 - eye health care
 - best vitamins for eye health
 - nutrition for eye health
 - how to maintain eye health
 - what to eat for eye health
 - eye health blog
 - supplements for eye health
 - diet for eye health
 - vitamin for eye health
 - for eye health
 - best food for eye health
 - vitamins for eye health macular degeneration
categories: 6 Common Eye Myths Debunked
date: 2021-04-16 13:42:38
---

![Eye Myths](/images/6-Common-Eye-Myths-Debunked.jpg)

Eye care and eye wellbeing are frequently hazy subjects in light of the fact that there are countless legends and misconceptions in regards to vision. Appropriate eye wellbeing is anything but a usually discussed subject. In case you're worried about your body's general wellbeing and health, don't skip vision care! There are precaution estimates you can take to keep up eye well-being on the off chance that you need to have a sharp vision or forestall further vision decay.

## 1. EAT CARROTS FOR GOOD VISION.
We've all heard it since youth. "Eat carrots. They're useful for your eyes!" Yes, carrots are indeed useful for your eyes, yet it's not carrots explicitly that are useful for eye wellbeing. Nutrient An in carrots is the thing that is known to be strong of eye wellbeing. 

You should burn through carrots consistently as they are phenomenal wellbeing food, yet there are so a lot more food sources that you can remember for your eating regimen that is magnificent for eye wellbeing since they likewise contain undeniable degrees of nutrient A. A portion of these food sources include:

 - Cantaloupe
 - Collard greens
 - Kale
 - Liver from organic meats
 - Organic eggs
 - Mango
 - Red bell peppers
 - Salmon
 - Sweet potatoes
 - Squash
 - Turnips

## 2. IF YOUR PARENTS HAVE POOR VISION, SO WILL YOU.
Hereditary qualities can unquestionably assume a part in your eye wellbeing. A few groups are brought into the world with helpless vision. Myopia, farsightedness, and astigmatism would all be able to be acquired. I have by and by required glasses for as far back as I can recollect. My kin and I all wear glasses and contact focal points as helpless vision runs in my mother's side of the family. 

Glaucoma and macular degeneration are age-related; notwithstanding, they have all the earmarks of being hereditarily passed down in an enormous part of the cases. While there isn't a lot you can do about hereditary qualities, you can in any case uphold your eye wellbeing however much as could reasonably be expected through a solid eating regimen and ordinary vision tests. You can track down my full rundown of tips beneath.

## 3. YOU DON’T NEED REGULAR EYE EXAMS IF YOU DON’T HAVE OBVIOUS VISION PROBLEMS.
Numerous individuals disregard to get their eyes and vision checked in the event that they don't have any undeniable vision issues. Nonetheless, vision tests are perhaps the best measure that you can take to keep up eye well-being. In the event that you have even a slight vision issue, wearing glasses or contact focal points will assist with forestalling eye strain and further vision decrease. 

The American Academy of Ophthalmology proposes visiting an ophthalmologist for a dream test like clockwork. On the off chance that you are 65 and more established, you should plan an eye test each year. Regardless of whether you don't think you have vision issues, you may require glasses for perusing or driving. Note that individuals will in general create eye conditions like glaucoma after the age of 40. It's significant not to disregard these tests so you can forestall or if nothing else know about any eye issues that may simply be beginning.

## 4. ELECTRONIC SCREENS WILL DAMAGE YOUR VISION.
Gazing at screens has not yet been demonstrated to harm vision. Be that as it may, consistently taking a gander at a screen for extensive stretches of time can cause eye weariness. You can begin to lose center, which can prompt temple agony and migraines. 

To decrease this strain, take parts from taking a gander at screens and take a gander at objects far away to give your astigmatic vision a rest. Utilize the 20-20-20 guideline when utilizing gadgets. 

Like clockwork, gaze at something that is 20 feet away for an entire 20 seconds! 

Television, PCs, and tablet screens ought to be at any rate 18 inches from your eyes, as indicated by the American Academy of Ophthalmology.

## 5. SQUINTING WILL DAMAGE YOUR VISION.
Squinting will not really harm your eyes; notwithstanding, notice on the off chance that you do squint. Squinting is an indication that you may have vision issues and you may require distance glasses. In the event that you squint incidentally or routinely, I exceptionally recommend booking an arrangement for an eye test.

## 6. THERE IS NOTHING YOU CAN DO TO PREVENT VISION LOSS.
Vision misfortune unquestionably appears to be an inescapable result for the vast majority of us, particularly as we age. While now and again it is out of your control, it's never past the point where it is possible to deal with your eye wellbeing and attempt to forestall vision decrease. In the event that you can shield your eyes since the beginning, far and away superior!

There are certain things you can do to protect your vision. My top suggestions are to:

 - Eat a healthy diet that is packed with nutrients including vitamin A.
 - Eat a diet containing other nutrients supporting eye health including antioxidants, omega 3s, vitamin C, vitamin E, and zinc.
 - Eat a diet that is blood sugar regulating. Imbalanced blood sugar levels can negatively affect vision and eye health.
 - Avoid smoking. Smoking increases the risk of age-related macular degeneration, glaucoma, cataracts, and diabetic retinopathy.
 - Know your family’s eye health history. Take preventative measures to try and prevent certain genetic diseases that run in your family.
 - Wear sunglasses to protect your eyes when you’re outside exposed to the sun.
 - If you wear contact lenses, take proper care of them and clean them regularly.


