---
title: Eye Safety At Home
author: Westpoint Optical
tags:
  - sun with sunglasses
  - sun glasses
  - eye care
  - westpoint optical stores
  - optical in brampton
  - eye test near me
  - westpoint eyewear
  - eye exam near me
  - contact lenses
  - kids eye wear
  - eye doctor
  - optical near me
  - contact lenses for kids
  - contact lens
  - contact lens case
  - eye sight
  - eye health 
  - westpoint optical eye wear
  - contact focal point
  - eye safety
  - eye doctor

Categories: Eye Safety At Home
date: 2021-03-27 20:37:09
---

![](/images/boy_pirate_med.jpg)

Most people do not think of eye safety at home, but half of all eye injuries happen in this “safe” place. What’s more, about 90% of all eye injuries could be prevented by wearing proper eye protection.

From cooking and cleaning to mowing the lawn and do-it-yourself home repairs, eye injuries occur every day while performing routine activities around the house. When tackling chores and tasks at home, it’s easy to become complacent about safety precautions. However, since one-quarter of all eye injuries result in time off work due to their severity, there are some safety tips worth following.

First of all, invest in a pair of Westpoint Optical CSA-certified safety glasses and use them around home. They are inexpensive and available at hardware and home building supply stores.

## Yard and Garden

  - Wear eye protection. Proper safety eyewear can protect against many risks, including flying dust and debris, and chemical splashes. Remember, regular corrective lenses or sunglasses won’t do the trick; use Westpoint Optical CSA-certified safety glasses that fit over your regular glasses.
  - Always read and follow the instructions and/or owner’s manual for safe handling of products (e.g. fertilizers, solvents) and equipment.  Keep tools in good condition.
  - Inspect and remove debris from the lawn before mowing, and from the surface of the snow before blowing. Inspect walkways and stairs to remove hazards there too.
  - Wash hands after completing tasks and chores, and before touching your eyes.
  - Trim all low hanging branches.

## Home Improvement

  - Again, wear eye protection! Screws, nails, and hand tools can become projectiles, and power tools can propel tiny chips into the air and into eyes.
  - Turn off power tools when an unprotected bystander is near, especially young children.

## Cleaning and Chemicals

  - Always read and follow manufactures’ instructions and warning labels when handling chemicals at home, and wash hands thoroughly afterward.
  - Do not mix cleaning agents.
  - Always point spray nozzles away from you.
  - Store all products, chemicals, and glue out of the reach of children.

## Toys and Children

  - Read all warnings and instructions on toys.
  - Avoid toys with sharp or rigid points, shafts, spikes, rods, and dangerous edges.
  - Keep toys intended for older children away from younger children.
  - Avoid flying toys and projectile-firing toys; be aware of items in playgrounds and play areas that pose potential eye hazards.
  - Keep BB guns away from children.
