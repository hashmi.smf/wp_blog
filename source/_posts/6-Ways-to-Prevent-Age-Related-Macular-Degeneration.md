---
title: 6 Ways to Prevent Age-Related Macular Degeneration (AMD)
author: Westpoint Optical
tags:
  - do i need glasses
  - how do you know if you need glasses
  - need glasses
  - signs yoiu need glasses
  - eye test
  - i need glasses
  - children glasses
  - how to wear glasses
  - do i need glasses test
  - glasses headache
  - baby with glasses
  - westpoint optical eyewear
  - optical near me
  - sunglasses
  - eye sight
  - eye exam cost brampton
  - optometrist near me
  - eye test
  - eye health tips
Categories: 6 Ways to Prevent Age-Related Macular Degeneration (AMD)
date: 2021-05-05 15:36:37
---
![age realted macular](/images/6-Ways-to-Prevent-Age-Related-Macular-Degeneration.jpg)
Age-related macular degeneration is a genuine condition that can undermine your vision and general prosperity. Described by the decay of the focal space of the retina called the macula which is answerable for central vision, the infection continuously decreases your focal vision. This influences the capacity to see fine subtleties, perceive faces, read, drive, sit in front of the TV and even utilize a PC. The infection frequently leaves some vision bringing about a condition called low vision, which is viewed as a type of legitimate visual impairment. 

AMD is the main source of vision misfortune in the more established populace and the numbers are required to increment as Americans and Canadians keep on living longer.

### What causes AMD and how can it be prevented?
As you can see by the name, the essential danger factor of AMD is age, especially over age 50. Caucasian ladies are the most widely recognized segment to be hit with this visual infection; family clinical history and having lighter-hued hair, skin, and eyes assume an enormous part also. Nonetheless, a few ways of life factors have been appeared to cause an expansion in AMD improvement; so there might be approaches to lessen your danger, regardless of whether you have a hereditary inclination. 

Indeed, the greater part of the controllable danger factors presents general wellbeing hazards that cause plenty of medical problems, so tending to them will support your general wellbeing and health, as well as shielding your eyes and vision from AMD. Here are 6 ways to prevent AMD and the vision loss that accompanies it:

**1. Stop Smoking**
Smoking, and surprisingly living with a smoker, have been appeared to fundamentally build your dangers of creating AMD to between 2-5 times the danger of non-smokers! In the event that you additionally have a genetic danger, smoking mixtures that hazard enormously.

**2. Get Active** 
Studies show that stoutness and a stationary way of life increment the danger of cutting edge macular degeneration that prompts huge vision misfortune. Keeping a solid weight and being dynamic can decrease your danger. That could be just about as simple as standard strolling, at any rate, 3 times each week for 30 minutes.

**3. Control Blood Pressure**
Since the eye contains numerous minuscule veins, hypertension can truly affect the wellbeing of your eyes. Have your circulatory strain checked by your PCP and follow any clinical exhortation you are given to lessen hypertension, regardless of whether that incorporates diet, exercise, or prescription.

**4. Choose a Healthy Diet**
An eating regimen wealthy in cancer prevention agents has been appeared to ensure against AMD. Cancer prevention agents can be found in bounty in dull green verdant vegetables like spinach, broccoli, kale, and collard greens, just as orange products of the soil like peppers, oranges, mango, and melon. Eating a wide scope of new products of the soil, 5-9 servings per day, just like fish, which contain Omega-3, and staying away from sugar and prepared food sources will assist with keeping your body sound from numerous points of view, including diminishing your danger of AMD.

**5. Use UV and Blue Light Protection**
Long-haul openness to UV beams from the sun and blue light (from advanced gadgets in addition to other things) have been connected to AMD. Ensure you wear shades each time you are presented to daylight and wear blue light-impeding glasses when you are seeing an advanced gadget or PC for expanded timeframes.

**6. Take Supplements**
Certain healthful enhancements have been appeared to moderate the movement of AMD and the vision misfortune that goes with it. This recipe of enhancements was created from a 10-year investigation of 3,500 individuals with AMD called the Age-Related Eye Disease Study (AREDS) and its replacement AREDS2. It isn't prescribed to accept supplementation as a protection measure but instead just in the event that you are determined to have transitional or progressed AMD.



