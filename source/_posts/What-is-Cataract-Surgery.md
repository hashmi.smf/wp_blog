title: What is Cataract Surgery?
author: Westpointoptical
tags:
  - eye glasses
  - eye exam near me
  - donate eyeglasses
  - eyeglasses for men
  - eyeglasses repair
  - clear eyeglasses
  - ray ban eyeglasses
  - cataract surgery side effects
  - cataract surgery recovery
  - kids eye glasses
  - eye exam near you
  - burberry eye glasses
  - designer eye glasses
  - mens eye glasses
  - round eye glasses
  - eye cataract surgery
  - cataract surgery recovery time
  - laser cataract surgery
  - cataract surgery cost
  - cataract surgery complications
categories:
  - What is cataract surgery?
date: 2021-03-18 17:15:00
---
## Cataract Surgery

Waterfall medical procedure is the expulsion of the regular focal point of the eye (likewise called "translucent focal point") that has built up an opacification, which is alluded to as a waterfall. Metabolic changes of the translucent focal point filaments over the long run lead to the advancement of the waterfall and loss of straightforwardness, causing hindrance or loss of vision. During a waterfall medical procedure, a patient's overcast common focal point is taken out and supplanted with an engineered focal point to reestablish the straightforwardness of the focal point.

![cataract surgery](/images/cataract-surgery.jpg)

Following careful expulsion of the common focal point, a counterfeit intraocular focal point embed is embedded (eye specialists say that the focal point is "embedded"). Waterfall medical procedure is for the most part performed by an ophthalmologist (eye specialist) in a mobile (as opposed to inpatient) setting, in a careful focus or emergency clinic, utilizing nearby sedation (either effective, peribulbar, or retrobulbar), as a rulemaking next to zero inconveniences the patient.

Well, more than 90% of activities are effective in reestablishing valuable vision, with a low entanglement rate. Outpatient care, high volume, negligibly intrusive, little entry point phacoemulsification with brisk post-operation recuperation has gotten the norm of care in waterfall medical procedure everywhere in the world.

## What are cataract?

Cataracts are a clouding of the eye’s internal lens that is both progressive and painless. The condition is a result of a protein buildup in the lens of the eye, which prevents light from easily passing through. This makes it hard to see clearly. As the disease progresses, cataracts can eventually cause blindness.

Cataracts typically develop slowly and don’t cause many symptoms until they begin to distinctly block light.

![cataract](/images/cataract-img.jpg)

When cataract symptoms do appear, they may include:  
  - Blurry or clouded vision
  - Abrupt changes in corrective lens prescription
  - Changes in the appearance of eye color
  - Problems with glare while driving at night or during the day
  - Double vision (similar to a superimposed image) in the affected eye
  - Sensitivity to light
 
While cataracts are most commonly related to aging, they can occur in younger people as well.

## How Can I Lower My Risk of Cataract?

Certain danger factors increment your possibility of creating waterfalls. These regularly include:
 - Diabetes
 - Excessive alcohol consumption
 - Family and medical history
 - Medications
 - Obesity
 - Smoking
 - UV ray exposure
 
To bring down your danger, think about diminishing your liquor consumption, quit smoking, start an activity program, eat food varieties plentiful in nutrients An and C, and wear 100% UV-obstructing shades.  
  
## Do i need Cataract surgery?

Waterfall medical procedure is quite possibly the most widely recognized methodology today. Indeed, the Canadian Academy of Ophthalmology appraises that 2 million individuals go through the method every year. 

During the technique, the specialist will tenderly eliminate the waterfall from the eye and supplant it with a fake intraocular focal point (known as an IOL). Since it's a typical strategy, a waterfall medical procedure is generally acted in an outpatient facility or in your eye specialist's office. There is no compelling reason to remain in an emergency clinic and you can ordinarily continue your typical exercises in only a couple of days.