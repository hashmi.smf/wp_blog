---
title: Aging Eyes and Driving Safety
author: Westpoint Optical
tags:
  - aging eye
  - eye safety
  - sun with sunglasses
  - sun glasses
  - eye care
  - westpoint optical stores
  - optical in brampton
  - eye test near me
  - westpoint eyewear
  - eye exam near me
  - contact lenses
  - kids eye wear
  - eye doctor
  - optical near me
  - contact lenses for kids
  - contact lens
  - contact lens case
  - eye sight
  - eye health 
  - westpoint optical eye wear
  - contact focal point
  - eye safety
  - eye doctor
  - frames
  - suitable frames
  - eyeglass frame
  - eyeglasses
  - glasses online
  - glasses frames
  - online eyeglasses
  - eye exam near me

Categories: Aging Eyes and Driving Safety
date: 2021-03-29 19:50:45
---

## 20 Ways Aging Changes Your Eyes

![Eye Sight and Driving](/images/middle-aged-man.jpg)

A recent survey found two out of three Canadians falsely believe vision loss is inevitable as we age. Sure, aging can affect your eyes — but vision loss is not the norm. For 2020: Year of the Eye, The Canadian Ophthalmological Society (COS) presents 20 common changes to vision and eye health that aging adults should watch for, and the best ways to protect your sight.

### Common signs of aging eyes

## 1. Trouble reading fine print
Farsightedness, or presbyopia, is much more common after the age of 40. This makes up-close activities, such are reading or sewing, more challenging. Reading glasses can help.

## 2. Difficulty seeing at night
Older adults may notice that their eyes take longer to adjust and focus in the dark than they used to. Studies have suggested that the eye’s rod cells, which are responsible for low light vision, weaken with age. That’s why driving becomes trickier at night, or during poor weather. The National Traffic Safety Administration recommends that older people limit driving to daylight hours.

## 3. Dry eyes
Older adults tend to produce less tears, an uncomfortable eye condition called dry eye. Dry eye is especially common among women who have gone through menopause. Depending on severity, your ophthalmologist will recommend the best treatment for you.

## 4. Objects blending into backgrounds
It may become more difficult to distinguish objects from backgrounds of similar color, such as milk in a white cup. This is called loss of contrast sensitivity. There are low vision techniques to help with this, such as using opposite colors around the house.

## 5. Red, swollen eyelids
Blepharitis, an inflammation of the eyelid, becomes more common due to hormonal changes as we age. Symptoms include red or swollen eyes, a crusty sensation around the eyelashes, or soreness.

## 6. Spots or floaters in your vision
The vitreous, or jelly-like substance filling the middle of the eye, can thicken or shrink as we age. When this happens, tiny clumps of gel can form and cause floaters in our vision. This is usually harmless, but should be discussed with an ophthalmologist.

## 7. Flashes of light
When people see occasional flashes of light in their vision, it is often a sign of aging. These flashes occur when the vitreous rubs or pulls on the retina. Like floaters, a sudden increase in frequency should be discussed with an ophthalmologist.

## 8. Glare sensitivity
Aging adults with certain eye conditions can become increasingly sensitive to glare. A good way to minimize the discomfort is to use a matte screen filter on digital devices, adjust lighting around the house, and make sure to cover your eyes with sunglasses and a wide-brimmed hat while outdoors.

## 9. Cataracts
Half of Canadians over the age of 75 develop cataracts. A cataract is when the lens inside of our eye become cloudy, making it difficult to see. Cataracts can be treated with surgery.

## 10. Age-related macular degeneration (AMD)
AMD is a common eye disease, usually found in adults over the age of 50. Although patients may not notice symptoms during early stages of the disease, central vision will eventually decline. Treatment varies depending on the type of AMD.

## 11. Glaucoma
People of all ages can be diagnosed with glaucoma, but the disease is most common among senior adults. The disease damages the optic nerve and can lead to blindness if not treated early. Since symptoms often go unnoticed, getting regular eye exams is the best preventative measure to protect yourself from vision loss.

## 12. Diabetic retinopathy
Diabetic retinopathy is another leading cause of vision loss among adults. When someone with diabetes experiences very high levels of blood sugar, the blood vessels in the retina can become damaged, and vision can be compromised. There are various treatment options for diabetic retinopathy. 

## 13. Ocular melanoma
Although ocular melanoma is rare, it is the most common eye cancer and is more common in adults as they age. Routine eye exams are particularly important for catching ocular melanoma since early symptoms often go unnoticed. Diagnosis begins with a dilated eye exam.

## 14. Falls can cause vision-threatening injuries
Falling becomes more likely as we age due to changes in balance and vision. This can lead to serious injuries, including eye injuries — which happen most often at home. Simple adjustments around the house can be done to minimize the risk of a fall, including cushioning sharp corners of furniture and home fixtures, securing railings, and making sure rugs and mats are slip-proof.

## 15. Sleep disruption
Research suggests our eyes absorb less blue light as we age. This is why our bodies often produce less melatonin in our later years and can disrupt our normal sleep-wake cycles. Sleep problems are also believed to be more common in those with glaucoma and diabetic eye disease.

### How to protect your eyes — and your overall health — as you age

## 16. Health problems might show up first in your eyes
Routine eye exams are not only important for getting ahead of silent eye diseases — they are important for ensuring your overall health. Health conditions such as high blood pressure, high cholesterol, vitamin deficiencies, and several diseases can be detected through the eye before any symptoms surface.  

## 17. Knowing your family's health history can protect your future 
Identifying your risk factors is important for preventing serious eye diseases from stealing your sight. Family history, ethnicity, age, and other factors should be discussed with your ophthalmologist during your routine eye exam.

## 18. Healthy living is especially important as you age
Exercising, eating well and other healthy habits have always been important for every aspect of your health. The consequences, however, are more often felt with age. Reducing the risk of certain health conditions, such as obesity or diabetes, will protect your eyes, too. 

## 19. Low vision tools can help
Low vision is not a normal part of aging, but for those who do experience vision loss from an eye disease, there are several low vision tools that can help with maintaining independence. These range from smartphone apps that read text aloud to hand-held magnifiers. A low vision rehabilitation team will help with personalized recommendations for making life easier.

## 20. Plan to get your eyes checked more often as you age
As you age, expect to get dilated eye exams more often to make sure all is well with your eye health. The Academy recommends all healthy adults get a baseline eye exam with an ophthalmologist by age 40. Seniors over the age of 65 should see an ophthalmologist every one to two years.

