---
title: Getting Used to a New Pair of Glasses
author: Westpoint Optical
tags: 
  - pair of glasses
  - perfect glasses	
  - types of eyeglasses	
  - pair of glasses	
  - pair of sunglasses	
  - eyeglass frames for men face shape	
  - magnetic glasses	
  - 2 pairs	
  - best pair	
  - which frame suits me	
  - cost of spectacles	
  - wearing spectacles side effects	
  - glasses synonyms	
  - eye exam near me	
  - optics near me	
  - spectacles parts	

Categories: Getting Used to a New Pair of Glasses
date: 2021-04-01 14:49:37
---

Getting used to a new pair of glasses may take a few days or even longer. To help you do it as quickly as possible, we’ve compiled a few tips from the eye care experts. Our goal is to help you enjoy a simple, straightforward and stress-free transition to new glasses.

![Getting Used to a New Pair of Glasses](/images/new-pair-of-glasses.jpg)

## Some Things to Consider

Our eyes work via our brains. When we switch eyeglasses or start wearing glasses for the first time, our eyes and brain need to get coordinated. In particular, if a prescription has changed, there may be an adjustment period, as the brain and eyes begin to work together in order to process vision properly. As long as your prescription is right for your eyes, this phase should pass quickly.

Sometimes, moving from bigger to more compact frames may also cause the same issue. During the adjustment phase, vision may not seem quite right. Certain people won’t deal with this issue – others will need to adjust. In time, your eyeglasses should become like a part of you – this means that you won’t notice them much or have any more adjustment-related issues.

In general, adjustment issues won’t last longer than fourteen days. If you’re new to wearing glasses, wearing them a lot will help the adjustment phase to pass quickly. So, be patient with the process and wear your glasses as much as your eye doctor tells you to. This may be all of the time or some of the time. He or she knows best.

If you’re still having adjustment issues after two weeks, it may be a sign that the frames and/or prescription aren’t right for you. We recommend visiting your optician in order to see if your lenses and frames are giving you the help that they should be.

## Avoid Buying Eyeglasses Online

One sure way to avoid excessive adjustment periods is to buy your eyeglasses in your own community, from a licensed optician or other eye care professional. Your optometrist likely sells eyeglasses right from his or her office and this will be a great place to buy them, as your optometrist has all of your records at the ready. You’ll be able to access the right lenses and frames that are nice and comfortable.

Nonetheless, it may take a few days to adjust.

If you do order glasses online, you won’t get the specialized care that an optometrist provides and this is a real downside. So, do consider where you buy when considering the adjustment period for new glasses – it will make a difference.

If your lenses are badly scratched and your eyeglass prescription has expired — or you simply want new glasses — schedule an eye exam.

**NEED AN EYE EXAM?** Find an [eye doctor near you](https://westpointoptical.ca/contact.html) and make an appointment.
