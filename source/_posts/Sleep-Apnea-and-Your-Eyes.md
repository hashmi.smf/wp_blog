---
title: Sleep Apnea and Your Eyes
author: Westpoint Optical
tags:
  - oral appliance sleep apnea
  - sleep apnea
  - sleep apnea mask
  - sleep apnea mouth guard
  - signs of sleep apnea
  - sleep apnea causes
  - central sleep apnea
  - can you die from sleep apnea
  - surgery for sleep apnea
  - obstructive sleep apnea
  - sleep apnea machine
  - sleep apnea and covid 19
  - eye exam near me
  - optical in brampton
  - eye infection
  - westpoint optical eye wear
  - trifocal lenses
  - eye doctor
  - optical in brampton
  - eye exam

Categories: Sleep Apnea & Your Eyes
date: 2021-03-31 14:14:01
---

## 5 Eye Conditions Associated With Sleep Apnea

![](/images/two-men-eyes-closeup.jpg)

Did you know that certain eye conditions can be associated with obstructive sleep apnea? Here we will discuss the possible conditions, what symptoms you may be experiencing with them, and how they are related to obstructive sleep apnea (OSA).

## Obstructive Sleep Apnea

Obstructive sleep apnea (OSA) is a serious yet common breathing disorder that occurs while sleeping.  During OSA, the airway becomes partially blocked, and events such as hypopnea (abnormally slow, shallow breathing) or apnea (absence of breathing) happen. Surprisingly, it is estimated that 80% of patients that have OSA are not diagnosed.

It has been well established that OSA is associated with many medical conditions such as cardiovascular disease, depression, diabetes, and dementia. Additionally, research has highlighted that OSA is associated with eye diseases and conditions.

## What eye diseases or conditions are linked to OSA?

Your eye doctor may see several eye diseases or conditions that have been related to OSA. These can include:

### Floppy Eyelid Syndrome 
Floppy Eyelid Syndrome (FES) is an eye condition where a person has an extremely large and floppy upper eyelid. This condition is regularly undiagnosed but can be bothersome for many patients. 

  - Symptoms- Patients will typically complain about eye irritation, redness, discharge, blurry vision, and may even describe occasions when their upper eyelids spontaneously evert upward (flip over) while they are asleep.
  - How floppy eyelid syndrome is associated with OSA- This is one of the most common eye conditions associated with OSA. Research has shown that more than 90% of patients with FES have OSA and it is believed to be caused by weakened connective tissue similar to that seen with OSA.

### Glaucoma
Glaucoma generally begins when fluid builds up in the front part of the eye, increasing the pressure within the eye and causing damage to the optic nerve. This eye disease is a leading cause of blindness among people over the age of 60, and it affects nearly 60 million people worldwide. 

  - Symptoms- This eye disease develops slowly and may not have any symptoms (vision loss) until it is quite advanced (Glaucoma Research Foundation, 2017).
  - How glaucoma is associated with OSA- The most common form of glaucoma is called primary open-angle glaucoma. According to new research from the Taipei Medi and cal University, sleep apnea is an independent risk factor for open-angle glaucoma. The findings from their studies have shown that individuals diagnosed with OSA were 1.67 times more likely to have open-angle glaucoma within five years of their diagnosis than those without the sleep condition. 

### Non-arteritic Anterior Ischemic Optic Neuropathy
This eye condition occurs when there is loss of blood flow to the optic nerve. 

  - Symptoms- Patients typically complain of significant loss of vision in one eye without any considerable pain. In many cases, this is reported to happen immediately upon waking up in the morning, with no change (better or worse) once it occurs.
  - How non-arteritic anterior ischemic optic neuropathy (NAION) is associated with OSA- The exact cause of the reduced blood flow to the optic nerve is unknown, but it is known that this occurs more often when a patient experiences sleep apnea. Recent research has shown that sleep apnea is a contributing factor of non-arteritic anterior ischemic optic neuropathy and 70 to 80 percent of patients with NAION have been found to have OSA.

### Central Serous Chorioretinopathy
According to the American Society of Retina Specialists, central serous chorioretinopathy (CSC) is an eye condition caused by an accumulation of fluid under the retina that causes detachment and vision loss. Vision loss in many individuals is temporary, but it may recur (American Society of Retinal Specialists, 2016).

  - Symptoms- The most common symptom is blurry vision in one eye.
  - How central serous retinopathy is associated with OSA- Currently, the causes of CSC are not entirely understood. However, research has shown that this condition occurs more frequently with patients repeatedly exposed to stress. Recent studies have looked at the correlation of OSA and CSC and have concluded that more than 50% of patients with CSC may be more likely than other adults to have OSA.


### Retinal Vein Occlusion
Retinal vein occlusion (RVO) is a blockage of the small veins that carry blood away from the retina. When blood flow from the retina is blocked, it is often caused by a blot clot is blocking the retinal vein. Because the vein is blocked, swelling (increased pressure), fluid leakage, and bleeding can occur within the eye. It is often called an “eye stroke”.

  - Symptoms- The symptoms of RVO can range from blurry vision to sudden, permanent loss of sight. This happens almost exclusively in one eye. Patients may also experience pain and dark spots or lines in their line of vision.
  - How retinal vein occlusion is associated with OSA- A recent study of 114 adult RVO patients found that sleep apnea was suspected in 73.9% of the patients that had previously been diagnosed with RVO. These researchers concluded that OSA is probably a risk factor associated with RVO and patient screening for symptoms associated with OSA is necessary.

![](/images/optometrist-examining-patient.jpg)

## CPAP Therapy and Eye Health

One of the most common forms of treatment for OSA is called CPAP therapy. This type of treatment can have side effects associated with eye health symptoms as well. Doctors have recognized that CPAP machines can have an impact on a person’s eyes because of several reasons.

 1. **Dry Eyes**– Even though CPAP machines have water compartments designed to avoid the emission of dry air, there is still considerable leakage of air from the vents and sides of these devices. This air can go beyond the eyelids and lead to an increase in ocular surface dryness and associated daytime symptoms.
 2. **Bacterial Conjunctivitis**- This is one of the most commonly encountered eye problems in medicine and has been shown to be caused by bacteria present in the mouth and nose that is then blown across the eyes.
**Increased Intraocular Pressure**- A study published in the journal Thorax, has shown that the use of a CPAP machine (for individuals with OSA) significantly increased intraocular pressure (IOP) when compared to the baseline measurements. They suggested that future studies should be conducted to assess the long-term implications of glaucoma progression in CPAP users.

## Diagnosis of Eye Disease and Conditions
Regular eye examinations are essential to detect any eye disorders and prevent potential vision loss. An ophthalmologist will be able to diagnose and treat any eye disorders you may have. If you have questions or concerns about your eye issues and the relationship to sleep disorders, be sure to speak with your optometrist or ophthalmologist at your next visit. If your eye doctor finds any eye diseases or disorders that are signs of sleep apnea, you can be referred to an accredited sleep disorder center for additional evaluation. It may be necessary to schedule a sleep study (polysomnogram) to assess for any symptoms of OSA.

Having a yearly checkup can help you preserve your eye health. Contact Westpoint Optical to learn more about how to keep your eyes healthy.

Call Westpoint Optical on 905-488-1626 to schedule an eye exam with our Brampton optometrist.

Alternatively book an appointment online here [CLICK FOR AN APPOINTMENT](https://westpointoptical.ca/contact.html)


