---
title: What You Need to Know About Glaucoma – The Sneak Thief of Sight
author: Westpoint Optical
tags:
 - glaucoma
 - glaucoma symptoms
 - glaucoma treatment
 - glaucoma meaning
 - what is glaucoma
 - symptoms of glaucoma
 - congenital glaucoma
 - glaucoma surgery
 - glaucoma surgery
 - glaucoma definition
 - glaucoma causes
 - journal of glaucoma
 - glaucoma eye
 - phacomorphic glaucoma
 - glaucoma test
 - treatment of glaucoma
 - glaucoma images
 - glaucoma disease
 - types of glaucoma
 - causes of glaucoma
Categories: What You Need to Know About Glaucoma – The Sneak Thief of Sight
date: 2021-05-13 17:45:00
---
![Glaucoma Sight](/images/What-You-Need-to-Know-About-Glaucoma-The-Sneak-Thief-of-Sight.jpg)

### What is Glaucoma?
Glaucoma, a quiet eye illness, is the most widely recognized type of irreversible visual impairment on the planet. It is really a gathering of infections that makes reformists harm the optic nerve.

 - Glaucoma is a progressive eye disease that can lead to permanent vision loss if not controlled. 
 - There is currently no cure for glaucoma, but there are many treatments available for stopping and slowing the progressive damage to the eye. Treatment, however, can’t reverse damage that is already done.
 - Glaucoma affects the optic nerve which sends visual information from your eye to your brain. 
 - Glaucoma is called the “Thief Sneak of Sight” because there are often no symptoms in the early stages such as pain or "pressure sensation" as one may expect, and by the time it is diagnosed there may already be permanent vision loss.
 - When vision loss occurs, peripheral vision is typically affected before central vision. As a result, glaucoma is a major public health issue because individuals usually do not notice any problem with vision until end stages of the disease when there is severe and irreversible vision loss.
 - Awareness and regular eye exams are key to early detection and preventing vision loss. 

### What Causes Glaucoma?
Glaucoma is caused by a buildup of natural fluid that doesn’t drain properly from the eye. The buildup of fluid can result in high pressure in the eye which is the most common cause of the condition. There are many types of glaucoma, which include:

**Chronic (open angle) glaucoma** occurs when pressure builds up over time, usually as a result of aging. This is the most common type of glaucoma. 

**Acute (angle closure) glaucoma** is an acute condition where pressure builds up suddenly and demands immediate medical attention. Symptoms include blurred vision, eye pain, headaches, seeing halos around lights, nausea and vomiting. 

**Secondary glaucoma** results from another eye disease, condition or a trauma to the eye. 

**Normal tension glaucoma** is when there is no build up of pressure but the optic nerve is still damaged. We are still not yet sure what causes this type of glaucoma. 

### Who is at Risk for Glaucoma?
Everybody is in danger of glaucoma anyway there are sure factors that improve the probability of building up the condition. Vision misfortune from glaucoma can be incredibly diminished when recognized and treated early which is the reason realizing your dangerous elements can assume a gigantic part in avoidance.

**Age**
Age is one of the biggest risk factors, as your chances of developing glaucoma increase significantly after the age of 40. In fact people over 60 years old are six times more likely to get the condition. 

**Ancestry and Family History**
Individuals from African American, Hispanic, Asian and Native American or Aboriginal Canadian descent are at increased risk. Family history is a very strong factor as the condition occurs twice as much in people with close relatives who have had glaucoma. 

**Previous Eye Injury, Traumas or Surgery**
Eye injuries, traumas or surgeries have been known to sometimes cause secondary glaucoma which can happen immediately after the injury or procedure, or even years later. Even childhood injuries can result in secondary glaucoma later in life. 

**Use of Steroids**
Studies show that prolonged steroid use is linked to increased elevated intraocular pressure which increases the risk of open-angle glaucoma. 

Certain medical and eye conditions such as diabetes, hypertension and high myopia (nearsightedness) also increase a person’s risk of developing glaucoma. 

### Glaucoma Treatment
While there is no cure for glaucoma, there are treatments to slow down the progression of the disease including eye drop medications, iridotomies, iridectomies, laser procedures and surgeries. 

### Glaucoma Prevention
Other than dealing with any fundamental conditions that may expand the danger of creating glaucoma, there is minimal one can do in the method of anticipation. You can anyway decrease your odds of enduring vision misfortune. The initial step is realizing the danger factors and indications of the condition (despite the fact that as referenced most cases have no side effects in the beginning phases until vision is as of now lost). 

The most ideal approach to forestall vision misfortune is to have customary far-reaching eye tests to check the wellbeing of your eyes and if your eye specialists endorse drugs for glaucoma, make a point to tenaciously accept them as coordinated. Your eye specialist will actually want to direct certain tests to recognize eye sicknesses like glaucoma before you even start to see manifestations. On the off chance that you have any of the danger factors above, notice it to your eye specialist, and consistently make certain to plan a yearly eye test, or as frequently as your eye specialist suggests, to check the wellbeing of your eyes and preclude any basic or creating eye conditions like glaucoma.




