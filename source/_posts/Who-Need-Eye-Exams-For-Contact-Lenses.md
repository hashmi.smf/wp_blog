---
title: Who Need Eye Exams For Contact Lenses
author: Westpoint Optical
tags: 
  - eye exam near me
  - eye exam
  - eye exam cost
  - free eye exam
  - online eye exam
  - how much does an eye exam cost
  - eye exam chart
  - walmart eye exam
  - iris
  - eye test
  - newlook
  - diabetics eye exams
  - eye clinic
  - how much does an eye exam cost in ontario
  - how long does eye exam take
  - eye examniation
  - pediatric eye doctor
  - eye check up
  - comprehensive eye exam
  - 
Categories: Who Need Eye Exmas For Contact Lenses
date: 2021-03-26 17:20:01
---

## Eye Exams for Contact Lenses

![Eye Exam For Contact Lenses](/images/eye-exma-for-contact-lenses.jpg)

Contact lenses are a great alternative to wearing eyeglasses. An often unknown fact is that not all patients wear contact lenses as their primary source of vision correction. Each patient is different, with some patients wearing contact lenses only on weekends, special occasions or just for sports. That is the beauty of contact lens wear, the flexibility it gives each individual patient and their lifestyle.

If you decide to opt for contact lens wear, it is very important that the lenses fit properly and comfortably and that you understand contact lens safety and hygiene. A contact lens exam will include both a comprehensive eye exam to check your overall eye health, your general vision prescription and then a contact lens consultation and measurement to determine the proper lens fit.

## The Importance of a Comprehensive Eye Exam

Whether or not you have vision problems, it is important to have your eyes checked regularly to ensure they are healthy and that there are no signs of a developing eye condition. A comprehensive eye exam will check the general health of your eyes as well as the quality of your vision. During this exam the eye doctor will determine your prescription for eyeglasses, however this prescription alone is not sufficient for contact lenses. The doctor may also check for any eye health issues that could interfere with the comfort and success of contact lens wear.

## The Contact Lens Consultation

The contact lens industry is always developing new innovations to make contacts more comfortable, convenient and accessible. Therefore, one of the initial steps in a contact lens consultation is to discuss with your eye doctor some lifestyle and health considerations that could impact the type of contacts that suit you best.

Some of the options to consider are whether you would prefer daily disposables or monthly disposable lenses, as well as soft versus rigid **gas permeable (GP)** lenses. If you have any particular eye conditions, such as astigmatism or dry eye syndrome, your eye doctor might have specific recommendations for the right type or brand for your optimal comfort and vision needs.

Now is the time to tell your eye doctor if you would like to consider colored contact lenses as well. If you are over 40 and experience problems seeing small print, for which you need bifocals to see close objects, your eye doctor may recommend multifocal lenses or a combination of multifocal and monovision lenses to correct your unique vision needs.

## Contact Lens Fitting

One size does not fit all when it comes to contact lenses. Your eye doctor will need to take some measurements to properly fit your contact lenses. Contact lenses that do not fit properly could cause discomfort, blurry vision or even damage the eye. Here are some of the measurements your eye doctor will take for a contact lens fitting:

**Corneal Curvature**
In order to assure that the fitting curve of the lens properly fits the curve of your eye, your doctor will measure the curvature of the cornea or front surface of the eye. The curvature is measured with an instrument called a keratometer to determine the appropriate curve for your contact lenses. If you have astigmatism, the curvature of your cornea is not perfectly round and therefore a “toric” lens, which is designed specifically for an eye with astigmatism, would be fit to provide the best vision and lens fit. In certain cases your eye doctor may decide to measure your cornea in greater detail with a mapping of the corneal surface called corneal topography.

**Pupil or Iris Size**
Your eye doctor may measure the size of your pupil or your iris (the colored area of your eye) with an instrument called a biomicroscope or slit lamp or manually with a ruler or card. This measurement is especially important if you are considering specialized lenses such as Gas Permeable (GP) contacts.

**Tear Film Evaluation**
One of the most common problems affecting contact lens wear is dry eyes. If the lenses are not kept adequately hydrated and moist, they will become uncomfortable and your eyes will feel dry, irritated and itchy. Particularly if you have dry eye syndrome, your doctor will want to make sure that you have a sufficient tear film to keep the lenses moist and comfortable, otherwise, contact lenses may not be a suitable vision option.

A tear film evaluation is performed by the doctor by putting a drop of liquid dye on your eye and then viewing your tears with a slit lamp or by placing a special strip of paper under the lid to absorb the tears to see how much moisture is produced. If your tear film is weak, your eye doctor may recommend certain types of contact lenses that are more successful in maintaining moisture.

**Contact Lens Trial and Prescription**
After deciding which pair of lenses could work best with your eyes, the eye doctor may have you try on a pair of lenses to confirm the fit and comfort before finalizing and ordering your lenses. The doctor or assistant would insert the lenses and keep them in for 15-20 minutes before the doctor exams the fit, movement and tearing in your eye. If after the fitting, the lenses appear to be a good fit, your eye doctor will order the lenses for you. Your eye doctor will also provide care and hygiene instructions including how to insert and remove your lenses, how long to wear them and how to store them if relevant.

## Follow-up

Your eye doctor may request that you schedule a follow-up appointment to check that your contact lenses are fitting properly and that your eyes are adjusting properly. If you are experiencing discomfort or dryness in your eyes you should visit your eye doctor as soon as possible. Your eye doctor may decide to try a different lens, a different contact lens disinfecting solution or to try an adjustment in your wearing schedule.
**Contact Lens Eye Exam Near You**
Wearing the correct contact lenses for your eyes allows you to enjoy all of the benefits of wearing contacts, while keeping your eyes healthy and comfortable. 
If you're already a contact lens wearer, visit your eye doctor at least once a year to make sure the lenses are still providing you with optimum vision and comfort.

Contact Westpoint Optical in Brampton to book your contact lens eye exam today!
If your daytime vision has begun to struggle or you are tired of dealing with the hassle of glasses and/or contacts, then give us a call, today. At Westpoint Optical, we are proud to offer the most cutting-edge technologies and treatments to help our patients see as clearly as possible.

To learn more about the procedures we offer,[visit our website.](https://westpointoptical.ca/contact.html)




