---
title: What is LASIK eye surgery
author: Westpoint Optical
date: 2021-03-20 14:53:16
tags: 
 - eye glasses
 - LASIK surgery 
 - what is LASIK surgery 
 - when we should do LASIK surgery

categories:
 - Lasik surgery
---

LASIK eye a medical procedure is the most popular and most regularly performed laser refractive medical procedure to address vision issues. Laser-aided situ keratomileusis (LASIK) can be an option in contrast to glasses or contact focal points.


During LASIK medical procedure, an extraordinary kind of slicing laser is utilized to unequivocally change the state of the arch molded clear tissue at the front of your eye (cornea) to improve vision. 

In eyes with ordinary vision, the cornea twists (refracts) light accurately onto the retina at the rear of the eye. Yet, with partial blindness (nearsightedness), farsightedness (hyperopia), or astigmatism, the light is twisted erroneously, bringing about obscured vision.

![LASIK surgery](/images/lasik-surgery.jpg )

### Why it's done?

LASIK surgery may be an option for the correction of one of these vision problems:

 - **Nearsightedness (myopia)**:At the point when your eyeball is somewhat more than typical or when the cornea bends too strongly, light beams center before the retina and obscure inaccessible vision. You can see protests that are close decently plainly, however not those that are far away.

 - **Farsightedness (hyperopia)**: At the point when you have a more limited than normal eyeball or a cornea that is too level, light concentrations behind the retina rather than on it. This makes close to vision, and now and then inaccessible vision, hazy.

 - **Astigmatism**: When the cornea curves or flattens unevenly, the result is astigmatism, which disrupts focus of near and distant vision.

In case you're thinking about LASIK medical procedure, you likely as of now wear glasses or contact focal points. Your eye specialist will converse with you about whether LASIK medical procedure or another comparable refractive method is an alternative that will work for you.

### Risks

Complications that result in a loss of vision are very rare. But certain side effects of LASIK eye surgery, particularly dry eyes and temporary visual problems such as glare, are fairly common.
These usually clear up after a few weeks or months, and very few people consider them to be a long-term problem.


**Risks of LASIK surgery include:**


 - **Dry eyes**: LASIK medical procedure causes an impermanent lessening in tear creation. For the initial a half year or so after your medical procedure, your eyes may feel uncommonly dry as they recuperate. Dry eyes can diminish the nature of your vision. 

    Your eye specialist may suggest eyedrops for dry eyes. In the event that you experience extreme dry eyes, you could choose another technique to get exceptional plugs placed in your tear pipes to keep your tears from emptying endlessly out of the outside of your eyes. 

 - **Glare, halos and double vision**: You may experience issues seeing around evening time after a medical procedure, which ordinarily endures a couple of days to half a month. You may see expanded light affectability, glare, coronas around brilliant lights, or twofold vision. 

   In any event, when a decent visual outcome is estimated under standard testing conditions, your vision in faint light, (for example, at nightfall or in haze) might be decreased indeed after the medical procedure than before the medical procedure. 

 - **Under Corrections**:If the laser eliminates too little tissue from your eye, you will not get the more clear vision results you were expecting. Under corrections are more normal for individuals who are partially blind. You may require another LASIK method inside a year to eliminate more tissue. 

 - **Over corrections**: It's additionally conceivable that the laser will eliminate a lot of tissue from your eye. Overcorrections might be more hard to fix than under rectifications. 

 - **Astigmatism**: Astigmatism can be brought about by lopsided tissue evacuation. It might require extra a medical procedure, glasses or contact focal points. 

 - **Flap Problems** : Collapsing back or eliminating the fold from the front of your eye during a medical procedure can cause intricacies, including disease and overabundance tears. The furthest corneal tissue layer may develop unusually under the fold during the recuperating interaction. 

 - **Regression**: Regression is the point at which your vision gradually changes back toward your unique solution. This is a more uncommon difficulty. 

 - **Vision loss or changes**: Once in a while, careful complexities can bring about loss of vision. A few group likewise may not see as pointedly or obviously as beforehand.
