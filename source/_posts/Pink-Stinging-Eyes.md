---
title: 'Pink, Stinging Eyes?'
author: Westpointoptical
tags:
 - pink eye
 - pink eye disease
 - pink eye makeup
 - pink eye meaning
 - home remedies for pink eye
 - what is pink eye
 - pink eye treatments
 - pink eye symptoms
 - pink eye home remedies
 - symptoms of pink eye
 - how long does pink eye last
 - eye drops for pink eye
 - pink eye treatment
 - how to cure pink eye at home fast
 - how to get rid of pink eye fast
 - pink eye causes
 - pink eye drops
 - pink eye coronavirus
 - how to get rid of pink eye
 - pink eye pictures
 - treatment for pink eye
 - antibiotic eye drops for pink eye
 - how do you get pink eye
 - pink eye images
 - pink eye in babies

categories: Pink, Stinging Eyes?
date: 2021-04-16 12:07:21
---

![Stinging Eyes](/images/Pink,-Stinging-Eyes?.jpg)

Conjunctivitis, otherwise called pink eye, is an irritation of the conjunctiva. The conjunctiva is the slender clear tissue that lies over the white piece of the eye and lines within the eyelid. 

Kids get it a ton. It very well may be profoundly infectious (it spreads quickly in schools and childcare), however, it's once in a while genuine. It's probably not going to harm your vision, particularly in the event that you discover it and treat it rapidly. At the point when you take care to forestall its spread and do every one of the things your PCP suggests, pinkeye clears up with no drawn-out issues.

## What Causes Pinkeye?
Several things could be to blame, including:

 - Viruses, including the kind that causes the common cold
 - Bacteria
 - Irritants such as shampoos, dirt, smoke, and pool chlorine
 - A reaction to eyedrops
 - An allergic reaction to things like pollen, dust, or smoke. Or it could be due to a special   type of allergy that affects some people who wear contact lenses.
 - Fungi, amoebas, and parasites

Pinkeye brought about by certain microorganisms and infections can spread effectively from one individual to another, yet is anything but a genuine wellbeing hazard whenever analyzed immediately. On the off chance that it occurs in an infant, however, tell a specialist immediately, as it very well may be contamination that undermines the child's vision.

## What Are the Types of Pinkeye?
 - **Viral strains**. Viral strains are the most well-known - and possibly the most infectious - structures. They will in general beginning in one eye, where they cause bunches of tears and a watery release. Inside a couple of days, the other eye gets included. You may feel a swollen lymph hub before your ear or under your jawbone.

 - **Bacterial strains** usually infect one eye but can show up in both. Your eye will put out a lot of pus and mucus.

 - **Allergic types** produce tearing, itching, and redness in both eyes. You might also have an itchy, runny nose.

 - **Ophthalmia neonatorum** is a severe form that affects newborns. It can be caused by dangerous bacteria. Get it treated right away to prevent permanent eye damage or blindness.

 - **Giant papillary** conjunctivitis is linked with the long-term use of contacts or an artificial eye (ocular prosthesis). Doctors think it’s an allergic reaction to a chronic foreign body in your eye.

## What Are the Symptoms of Pinkeye?
They depend on the cause of the inflammation, but may include:

 - Redness in the white of the eye or inner eyelid
 - Swollen conjunctiva
 - More tears than usual
 - Thick yellow discharge that crusts over the eyelashes, especially after sleep. It can make your eyelids stick shut when you wake up.
 - Green or white discharge from the eye
 - Itchy eyes
 - Burning eyes
 - Blurred vision
 - More sensitive to light
 - Swollen lymph nodes (often from a viral infection)

## What’s the Treatment for Pinkeye?
The treatment depends on the cause.

**Viruses.** This sort of pinkeye frequently results from the infections that cause a typical virus. Similarly as a virus should run its course, the equivalent is valid for this type of pinkeye, which normally keeps going from 4 to 7 days. Keep in mind, it tends to be exceptionally infectious, so do all that you can to forestall its spread. Anti-microbials won't resist anything brought about by an infection.

**Bacteria.**Microbes. In the event that microbes, including those identified with STDs, caused your pink eye, you'll take anti-infection agents as eye drops, salves, or pills. You may have to apply eye drops or salves to your eyelid 3 to 4 times each day for 5 to 7 days. You would take pills for a few days. The contamination ought to improve within seven days. Take or utilize the meds as educated by your PCP, regardless of whether the indications disappear.

**Irritants.** For pinkeye brought about by a bothering substance, use water to wash the substance from the eye for 5 minutes. Your eyes should start to improve inside 4 hours. In the event that your conjunctivitis was brought about by corrosive or antacid material like dye, quickly flush the eyes with bunches of water and summon your primary care physician right.








