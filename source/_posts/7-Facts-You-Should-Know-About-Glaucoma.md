---
title: 7 Facts You Should Know About Glaucoma
author: Westpoint Optical
tags:
 - glaucoma
 - glaucoma symptoms
 - glaucoma treatment
 - glaucoma meaning
 - what is glaucoma
 - symptoms of glaucoma
 - congenital glaucoma
 - glaucoma surgery
 - glaucoma surgery
 - glaucoma definition
 - glaucoma causes
 - journal of glaucoma
 - glaucoma eye
 - phacomorphic glaucoma
 - glaucoma test
 - treatment of glaucoma
 - glaucoma images
 - glaucoma disease
 - types of glaucoma
 - causes of glaucoma
categories: 7 Facts You Should Know About Glaucoma
date: 2021-04-07 12:58:23
---
![Glaucoma](/images/7-Facts-You-Should-Know-About-Glaucoma.jpg)

Glaucoma is a genuine eye illness that can prompt vision misfortune. While all things considered, the vast majority don't consider glaucoma except if they've been determined to have it, it's an infection that we should all know and comprehend. Some basic misinterpretations about glaucoma can prompt disarray and determination after vision misfortune has effectively begun to happen. For instance, a few groups think glaucoma just influences grown-ups in their 70s and 80s.

Understanding more about glaucoma may protect your eyes and save your vision. Below are seven facts you may not know about glaucoma.

## 1. Glaucoma Is Quite Common
As indicated by the Glaucoma Research Foundation, around 3,000,000 individuals in the United States have glaucoma. The establishment likewise gauges that up to 12 percent of all instances of visual impairment are because of glaucoma. The National Eye Institute assesses that the number will twofold in the following 25 years. 

Just around 50% of individuals with glaucoma realize they have the sickness. Despite the fact that glaucoma is a typical eye illness, numerous individuals don't see much about the condition. A review by the Glaucoma Research Foundation tracked down that solitary 20% of individuals realized glaucoma prompted expanded eye pressure. The vast majority likewise figured they would have early indications. 

The absence of side effects, alongside an absence of mindfulness about the condition, regularly forestalls an early determination. Without treatment, glaucoma can prompt a deficiency of side vision, and at last, focal vision is additionally lost. 

## 2. Glaucoma Causes Damage to the Optic Nerve & Can Lead to Vision Loss
Glaucoma prompts an expanded pressing factor in the eye, which packs and harms the optic nerve. The eye produces liquid ceaselessly, which depletes out of the eye to keep the pressing factor consistent. In the event that an excess of liquid is created or it doesn't deplete as expected, liquid forms. 

The development of liquid inside the eye expands the pressing factor in the eye, which at that point pushes on the optic nerve, causing harm. The optic nerve conveys messages to the mind, which are then deciphered as the pictures we see. On the off chance that the optic nerve is harmed, it can cause fractional or complete vision misfortune. Lost vision can't be reestablished once glaucoma harms the optic nerve. 

## 3. Certain Factors Increase Risk of Developing Glaucoma
Despite the fact that anybody can create glaucoma, certain components increment your odds of building up the infection. Understanding your danger factors permits you to know how habitually you ought to have an exhaustive eye test to get the condition early. 

Hazard factors for creating glaucoma incorporate long haul utilization of corticosteroid eye drops, having a parent with the condition, and having certain ailments, for example, hypertension and diabetes. Individuals who are Hispanic or African American are additionally at a higher danger of creating glaucoma.

## 4. Glaucoma Does Not Only Affect Elderly Adults
One of the principal misinterpretations about glaucoma is that it just influences individuals in their later years. In spite of the fact that glaucoma is more normal in individuals more than 60, it can influence more youthful grown-ups. Those with hazard factors for creating glaucoma are particularly in danger of building up the infection at a more youthful age.

## 5. Early Detection Is the Key to Reducing the Chances of Vision Loss from Glaucoma
Manifestations of glaucoma, like foggy vision and diminished side vision, frequently don't happen until the harm is as of now done. Truth be told, changes in vision might be one of the main side effects. Luckily, your eye specialist can recognize glaucoma before you experience side effects. One of the indications of glaucoma is expanded eye pressure. A thorough eye test performed by your ophthalmologist can recognize expanded eye pressure.

## 6. Although There Is No Cure, Glaucoma Can Be Treated
Exploration proceeds to forestall and treat glaucoma. As of now, there is no fix, yet early treatment may moderate the movement of the sickness and save vision. The most well-known treatment is eye drops, which lowers eye pressure by either advancing liquid seepage from the eye or decreasing the measure of liquid the eye makes. Extra medicines incorporate laser techniques and customary medical procedures.

## 7. Certain Yoga Poses May Increase Eye Pressure & May Need to Be Avoided with Glaucoma
Yoga has become an inexorably famous type of activity for individuals, everything being equal. In the event that you have glaucoma, certain postures may expand the pressing factor in your eyes. As indicated by the American Academy of Ophthalmology, modified yoga positions, like a headstand, descending canine posture, and forward curve may build eye pressure in individuals with glaucoma and ought to be evaded. That doesn't mean you need to surrender yoga on the off chance that you have glaucoma. You simply need to alter positions where your head is underneath your body.

Remember, early detection is your best bet to prevent vision loss due to glaucoma. If you would like to schedule an eye exam with one of [our ophthalmologists](https://westpointoptical.ca/contact.html) who specializes in glaucoma or have any questions, please call our office at **905-488-1626**.



